/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011,2012,2018,2020  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Based in part on knighty's public domain nanomb1/nanomb2 code from:
    <https://fractalforums.org/f/28/t/277/msg8132#msg8132>
    <https://fractalforums.org/f/28/t/277/msg7983#msg7983
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <qd/dd_real.h>
#include <qd/qd_real.h>
#include <qd/fpu.h>
#include <list>
#include <pthread.h>

#include "mp_real.h"

#define NANOMB2

#define CHANNELS 5

#ifdef NANOMB2
// m.cpp 2018 Knighty
// based on some code by Claude Heiland-Allen
// LICENSE: public domain, cc0, whatever
// acceleration method for rendering Mandelbrot set
// based on approximation of n iterations around minibrot of period n using bivariate polynomial
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// compile:
//  g++ -std=c++11 -Ofast -mfpmath=387 -march=native mb.cpp -lmpfr -lgmp -Wall -Wextra -pedantic -fopenmp
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// run:
//  ./a --help
// view results:
//  display out.ppm
//#undef RC_INVOKED
//#undef __STRICT_ANSI__
//#include <float.h>

//#include <cassert>
#include <cstdlib>
#include <cstring>

#include <limits>

#include <iostream>
#include <fstream>

#if 0
#include <complex>
#define COMPLEX complex
#else
//A little problem with mpreal which includes <complex> so I had to change the name of the custom complex class to fcomplex
#include "complex.h"
#define COMPLEX fcomplex
#endif

#include <vector>

#include "mp_real.h"

using namespace std;

// type aliases
typedef int N;


typedef mp_real R_hi;

#if 0
#include <boost/multiprecision/mpfr.hpp>
typedef boost::multiprecision::number<boost::multiprecision::mpfr_float_backend<60>> R_lo; // precision in base 10 digits...
const int R_lo_mantissa_precision_bits = 200;
inline R_lo r_lo(const R_hi &z)
{
  return R_lo(z.m);//MPFR_RNDN);//mpfr_get_ld(z.backend().data(), MPFR_RNDN);
}
#else
typedef long double R_lo;
const int R_lo_mantissa_precision_bits = 64;
inline R_lo r_lo(const R_hi &z)
{
  return R_lo(z);
}
#endif

typedef COMPLEX<R_lo> C_lo;
typedef COMPLEX<R_hi> C_hi;

#define pi 3.141592653589793

#define TABSIZ 128
//--------------------------------------------------------------------------------------------------------
// Conversion routines

inline R_lo r_lo(const char *s)
{
  return strtold(s, NULL);
}

inline C_lo c_lo(const C_hi &z)
{
  return C_lo(r_lo(real(z)), r_lo(imag(z)));
}

//--------------------------------------------------------------------------------------------------------
// simple RGB24 image
class image
{
public:
  N width;
  N height;
  vector<uint8_t> rgb;

  // construct empty image
  image(N width, N height)
  : width(width)
  , height(height)
  , rgb(width * height * 3)
  { };

  // plot a point
  void plot(N x, N y, N r, N g, N b)
  {
    N k = (y * width + x) * 3;
    rgb[k++] = r;
    rgb[k++] = g;
    rgb[k++] = b;
  };

  // save to PPM format
  void save(const char *filename)
  {
    FILE *out = fopen(filename, "wb");
    fprintf(out, "P6\n%d %d\n255\n", width, height);
    fwrite(&rgb[0], width * height * 3, 1, out);
    fflush(out);
    fclose(out);
  }
};
//--------------------------------------------------------------------------------------------------------
class refClass{
	N m_n;
	vector<C_lo> m_v;
public:
	refClass(): m_n(0), m_v(0) {}
	void adde(C_lo c){ m_v.push_back(c); m_n++;}
	C_lo & operator[](N i){ return m_v[i % m_n];}
};

class perturbationClass{
	C_lo m_d0;
	C_lo m_d;
	N    m_n0;
	R_lo m_col;
	bool m_escaped;
public:
	perturbationClass(C_lo d0, C_lo d, N n0): m_d0(d0), m_d(d), m_n0(n0), m_col(0), m_escaped(false) {}
	void run(refClass &ref, N maxiter){
		for(N i=m_n0; i < maxiter; i++){
			C_lo zn(ref[i]);
			m_d = m_d * (R_lo(2) * zn + m_d) + m_d0;
			R_lo r(norm(zn + m_d));
			if (r > R_lo(256)){
				using std::log;
				m_escaped = true;
				m_col = R_lo(i+1) - log(log(r))/log(R_lo(2));
				return;
			}
		}
	}
	R_lo getCol(){return m_col;}
	bool haveEscaped(){return m_escaped;}
};


class perturbationClassD{
	C_lo m_d0;
	C_lo m_d;
	C_lo m_dd;
	N    m_si;
	N    m_n0;
	R_lo m_col;
	bool m_escaped;
	int m_iters;
	float m_trans;
public:

	perturbationClassD(C_lo d0, C_lo d, C_lo dd, N si, N n0): m_d0(d0), m_d(d), m_dd(dd), m_si(si), m_n0(n0), m_col(0), m_escaped(false) {}
	
	//Experimental: Usually, after series approximation delta_0 (m_d0 here) have no effect.
	//              Here we do a first loop where we add delta_0 then, when delta_0 is too small,
	//             a second loop is performed where delta_0 is not used anymore. (idea from Pauldelbrot)
	// It works! ToDo: ...
	void run1(refClass &ref, N maxsi, N maxiter){
		N si = m_si, i = m_n0;
		for( ; si < maxsi && i < maxiter; si++, i++){
			C_lo zn(ref[i]);
			if(abs(m_d0.re)+abs(m_d0.im) < 1e-20 * (abs(zn.re)+abs(zn.im))) break;
			m_dd = R_lo(2) * m_dd * (m_d + zn) + R_lo(1);
			m_d = m_d * (R_lo(2) * zn + m_d) + m_d0;
			R_lo r(norm(zn + m_d));
			if (r > R_lo(1e10)){
				using std::log;
				using std::sqrt;
				m_escaped = true;
				R_lo m = R_lo(i+1) - log(log(r))/log(R_lo(2));
				m_iters = int(floor(m));
				m_trans = float(m - m_iters);
				m_col = R_lo(2) * sqrt(r) * log(r) / abs(m_dd);
				return;
			}
		}
		for( ; si < maxsi && i < maxiter; si++, i++){
			C_lo zn(ref[i]);
			m_dd = R_lo(2) * m_dd * (m_d + zn);// + R_lo(1);
			m_d = m_d * (R_lo(2) * zn + m_d);// + m_d0;
			R_lo r(norm(zn + m_d));
			if (r > R_lo(1e10)){
				using std::log;
				using std::sqrt;
				m_escaped = true;
				R_lo m = R_lo(i+1) - log(log(r))/log(R_lo(2));
				m_iters = int(floor(m));
				m_trans = float(m - m_iters);
				m_col = R_lo(2) * sqrt(r) * log(r) / abs(m_dd);
				return;
			}
		}
	}
	void run(refClass &ref, N maxsi, N maxiter){
		for(N si = m_si, i = m_n0 ; si < maxsi && i < maxiter; si++, i++){
			C_lo zn(ref[i]);
			m_dd = R_lo(2) * m_dd * (m_d + zn) + R_lo(1);
			m_d = m_d * (R_lo(2) * zn + m_d) + m_d0;
			R_lo r(norm(zn + m_d));
			if (r > R_lo(1e10)){
				using std::log;
				using std::sqrt;
				m_escaped = true;
				R_lo m = R_lo(i+1) - log(log(r))/log(R_lo(2));
				m_iters = int(floor(m));
				m_trans = float(m - m_iters);
				m_col = R_lo(2) * sqrt(r) * log(r) / abs(m_dd);
				return;
			}
		}
	}
	R_lo getCol(){return m_col;}
	int getIters(){return m_iters;}
	float getTrans(){return m_trans;}
	bool haveEscaped(){return m_escaped;}
};

//--------------------------------------------------------------------
// Class for probes used to determine when to stop SA computations
// Grabbed from superMB. For future use
//--------------------------------------------------------------------
class probeClass
{
    C_lo m_dzz;
public:
    C_lo m_dc;// delta c
    C_lo m_dz;// current delta z
	
	//Constructors
    probeClass(){}
    probeClass(C_lo dc, C_lo dz):m_dc(dc),m_dz(dz){ m_dzz = C_lo(0);}
    probeClass(const probeClass & pC){ m_dc = pC.m_dc; m_dz = pC.m_dz; m_dzz = pC.m_dzz;}
    
	//Do one iteration. the iterate is stored in the private member variabe m_dzz. One must call apply() 
	//member function to store the new iterate in m_dz. 
	void advance(const C_lo &rz){
        m_dzz = m_dz * (R_lo(2) * rz + m_dz) + m_dc;
    }
	
	//In case we need to rebase the SA to a new point.
    void shift(const C_lo &s, const C_lo &sz){
        m_dc -= s;
        m_dz -= sz;
        m_dzz = m_dz;//necessary to avoid the "floral fantasy" glitch. Also because areProbesOk uses dif2ex() below which compares against d_zz.
    }
	
	//
    void apply(){m_dz = m_dzz;}
    
	//Just computes the difference between m_dzz and the given point.
	C_lo dif2ex(const C_lo &ex){ return m_dzz - ex;}
};

//--------------------------------------------------------------------
// Stores informations about the roots that are found during SA.
// For future possible use.
//--------------------------------------------------------------------
class rootInfoClass
{
public:
    N n; //At which iteration this root was found
    N mult; //Multiplicity of the root... for later use
    C_lo RPos; //position of the root w.r.t. current reference point
    rootInfoClass(){}
    rootInfoClass(N _n, N _mult, const C_lo &_RPos):
        n(_n), mult(_mult), RPos(_RPos) {}
};

//class biPolyClass;//Forward declaration

//--------------------------------------------------------------------------------------------
//Polynomial class
//--------------------------------------------------------------------------------------------
class polyClass{
	N m_m; //highest exponent
	C_lo tab[TABSIZ];//to store the coefficients. It is more than big enought.
public:
	//Constructor
	polyClass(N m): m_m(m) {
		for(N l=0; l <= m_m; l++)
			tab[l] = C_lo(0);
	}
	//Copy constructor
	polyClass(const polyClass& p): m_m(p.m_m) {
		for(N l=0; l <= m_m; l++)
			tab[l] = p.tab[l];
	}
	
	//evaluation function. It would be nice to add an ()-operator.
	C_lo eval(C_lo u){
		C_lo r(0);
		C_lo ui(1);
		for(N i=0; i <= m_m; i++){
			r += tab[i] * ui;
			ui *= u;
		}
		return r;
	}
	
	//evaluat derivative.
	C_lo evalD(C_lo u){
		C_lo r(0);
		C_lo ui(1);
		for(N i=1; i <= m_m; i++){
			r += R_lo(i) * tab[i] * ui;
			ui *= u;
		}
		return r;
	}
	
	//Gives the nearest root to the 0. To use if and when applicable (that is the reference is near 0... atom domain thing!)
	//Newton should do the job (otherwise IA-Newton ?).
	C_lo getRoot(){
		C_lo rt(0);
		//R_lo t = abs(eval(rt));
		for(N i=0; i<30; i++){
			C_lo num = eval(rt);
			C_lo den = evalD(rt);
			C_lo delta = num / den;
			num = rt;
			rt -= delta;
			if( rt.re == num.re && rt.im == num.im) break;
		}
		return rt;
	}
	
	//Shift the polynomial to the given (relative) origin point
	//given a polynomial p(z) this function returns the polynomial q(z)==p(z+shift)
	polyClass shift(C_lo shift){
		polyClass res(m_m);
		R_lo bino[TABSIZ];//vector<R_lo> bino(m_m + 2);//bino could be precomuted.
		for(int i=0;i<=m_m;i++)
			bino[i]=1;
		for(int i=0; i<=m_m; i++){
			C_lo v(0);
			for(int j=m_m-i; j>=0; j--)
				v = tab[j+i] * bino[j] + shift * v;
			for(int j=1; j<=m_m; j++)
				bino[j] += bino[j-1];
			res.tab[i] = v;
		}
		return res;
	}
	
	friend class biPolyClass;
};

//--------------------------------------------------------------------------------------------
//Bivariate polynomial class. Used for SSA.
//--------------------------------------------------------------------------------------------
class biPolyClass {
	N    m_m, m_n;
	C_lo tab[TABSIZ][TABSIZ];
	C_lo ttab[TABSIZ][TABSIZ];
	C_lo m_shift;
	N    m_period;
	R_lo m_escRadius;
	
	//copy tab into ttab. used for the squaring.
	void mcopy(){
		for(N l=0; l <= m_m; l++)
			for(N c=0; c<= m_n; c++)
				ttab[l][c] = tab[l][c];
	}
	
	//for the squaring operation: ttab will be the square of tab
	C_lo csqrc(N k, N l){
		C_lo v(0);
		for(N i=0; i <= k; i++)
			for(N j=0; j <= l; j++)
				v += ttab[i][j] * ttab[k-i][l-j];
		return v;
	}
public:
	//Constructor
	biPolyClass(N m, N n): m_m(m), m_n(n), m_shift(0), m_period(0), m_escRadius(0) {
		for(N l=0; l <= m_m; l++)
			for(N c=0; c<= m_n; c++)
				tab[l][c] = C_lo(0);
		tab[1][0] = C_lo(1);
	}
	
	C_lo getShift(){ return m_shift;}
	N    getPeriod(){ return m_period;}
	void setPeriod(N period){ m_period = period;}
	void setEscRadius(R_lo multiplier = R_lo(.1)){
		m_escRadius = multiplier * getRadius();
	}
	R_lo getEscRadius(){ return m_escRadius;}
	
	//squaring
	void sqr(){
		mcopy();
		for(N i=0; i <= m_m; i++)
			for(N j=0; j <= m_n; j++)
				tab[i][j] = csqrc(i,j);
	}
	
	//Apply one Mandelbrot iteration
	void cstep(C_lo z){
		sqr();
		tab[0][0]  = z;
		tab[0][1] += C_lo(1);
	}
	
	//Evaluate the SSA polynomial
	C_lo eval(C_lo u, C_lo v){
		//v -= m_shift;//Take into account the shift
		C_lo r(0);
		C_lo ui(1);
		for(N i=0; i <= m_m; i+=2){
			C_lo vj(ui);
			for(N j=0; j <= m_n; j++){
				r += tab[i][j] * vj;
				vj *= v;
			}
			ui *= u*u;
		}
		return r;
	}
	
	//Evaluate the derivative wrt c
	C_lo eval_dc(C_lo u, C_lo v){
		//v -= m_shift;
		C_lo r(0);
		C_lo ui(1);
		for(N i=0; i <= m_m; i+=2){
			C_lo vj(ui);
			for(N j=1; j <= m_n; j++){
				r += R_lo(j) * tab[i][j] * vj;
				vj *= v;
			}
			ui *= u*u;
		}
		return r;
	}
	
	//Evaluate the derivative wrt z
	C_lo eval_dz(C_lo u, C_lo v){
		//v -= m_shift;
		C_lo r(0);
		C_lo ui(u);
		for(N i=2; i <= m_m; i+=2){
			C_lo vj(C_lo(i) * ui);
			for(N j=0; j <= m_n; j++){
				r += tab[i][j] * vj;
				vj *= v;
			}
			ui *= u*u;
		}
		return r;
	}
	
	//To see the coefficients
	void print(){
		for(N i=0; i <= m_m; i++){
			for(N j=0; j <= m_n; j++){
				cout << "i: " << i << "\tj: " << j << "\tval: " << tab[i][j] << endl;
			}
			cout << "-----------------------" << endl;
		}
	}

	void setEscRadiusRaw(R_lo escRadius, R_lo multiplier = R_lo(1)){
		m_escRadius = multiplier * escRadius;
	}
	
	//gives the escape radius for this SSA.
	//based on polynomial roots properties: Rouché theorem.
	//ToDo: Document the method by which we find the radius.
	R_lo getRadius(){
		if(abs(tab[0][2])==0) return 1e30;
		return abs(tab[0][1])/abs(tab[0][2]);//Is actually accurate enough
		/*R_lo r(0);
		for(int i = 0; i < 10; i++){
			C_lo den(0);
			R_lo rr(1);
			for(int j = 2; j <=m_n; j++){
				den += tab[0][j] * rr;
				rr  *= r;
			}
			r = abs(tab[0][1])/abs(den);
		}
		return r;*/
	}
	//version that gives an estimate of the bail out radius in dynamic plane depending on v.
	R_lo getRadius(C_lo v){
		C_lo t2(0), t4(0), vi(1);
		for(N i = 0; i <= m_n; i++){
			t2 += tab[2][i] * vi;
			t4 += tab[4][i] * vi;
			vi *= v;
		}
		return abs(t2)/abs(R_lo(2)*t4);//Is actually accurate enough... 
	}
	
	//Just give the part that depends on v only. It can be used as the classical series approximation.
	polyClass getSAPoly(){
		polyClass SApol(m_n);
		for(N i=0; i<=m_n; i++)
			SApol.tab[i] = tab[0][i];
		return SApol;
	}
	
	//Partially evaluate the biPoly for the given v value. 
	polyClass getEvalPoly(C_lo v){
		polyClass SApol(m_m);
		//v -= m_shift;
		for(N i=0; i<=m_m; i++){
			C_lo vj(1);
			C_lo r(0);
			for(N j=0; j <= m_n; j++){
				r  += tab[i][j] * vj;
				vj *= v;
			}
			SApol.tab[i] = r;
		}
		return SApol;
	}
	
	//Partially evaluate the biPoly for the given v value. This is for derivative evaluation.
	polyClass getEvalPoly_dz(C_lo v){
		polyClass SApol = getEvalPoly(v);
		//v -= m_shift;
		for(N i=0; i<m_m; i++){
			SApol.tab[i] = SApol.tab[i+1] * R_lo(i+1);
		}
		SApol.m_m -= 1;
		return SApol;
	}
	
	//Partially evaluate the biPoly for the given v value. This is for derivative evaluation.
	//To implement
	polyClass getEvalPoly_dc(C_lo v){
		polyClass SApol(m_m);
		//v -= m_shift;
		for(N i=0; i<=m_m; i++){
			C_lo vi(1);
			C_lo r(0);
			for(N j=1; j<=m_n; j++){
				r  += R_lo(j) * vi * tab[i][j];
				vi *= v;
			}
			SApol.tab[i] = r;
		}
		return SApol;
	}
	
	//gives a shifted biPoly.We shift only wrt v param therefore we will use the shift function of polyClass.
	biPolyClass getShiftedBP(C_lo sv){
		biPolyClass SBP(m_m,m_n);
		for(N i=0; i<=m_m; i++){
			polyClass pol(m_n);
			for(N j=0; j<=m_n; j++)
				pol.tab[j] = tab[i][j];
			polyClass popol = pol.shift(sv);
			for(N j=0; j<=m_n; j++)
				SBP.tab[i][j] = popol.tab[j];
		}
		SBP.m_shift = m_shift + sv;//ToDo: verify if itis necessary to do add m_shift . This function is meant to be called only for the main SSA where shift==0. Just in case...
		return SBP;
	}
};

//---------------------------------------------------------------------------------------------------
//Iterate point till escape then return a "color" (not pretty at all)
//ToDo: use the closure of the biPoly (fp) for the current d0 because it is constant
//Nota 1: It is interesting that Bout is approximately the square root of the "size" of the minibrot
//Nota 2: It is also interesting that the bail out ardius around 0 -in dynamic plane- is the square root of the bailout around the minibrot -in the parameter space-
//Nota 3: In reality Bout depends on d0 (the v parameter (in biPolyClass) corresponding to c. we have d0==v and [C]=c+v )
//        One could recompute Bout for each pixel.
//---------------------------------------------------------------------------------------------------
R_lo iteratePt( C_lo d0, biPolyClass &fp, refClass &ref, N period, N maxiter, R_lo Bout, N &si ){
	C_lo d(0);
	C_lo ld(0);
	N i(0);
	si = N(0);
	if(abs(d0)<Bout){
		while(i<maxiter && norm(d)<Bout){//notice that we are comparing d^2 with Bout: norm(d)<Bout <=> d<sqrt(Bout)
			d = fp.eval(d,d0);
			i+=period;
			si++;
		}
	}
	if(i>maxiter)
		return R_lo(-1);
	perturbationClass p(d0,d,i);
	p.run(ref,maxiter);
	if(p.haveEscaped())
		return p.getCol();
	else
	    return R_lo(-1);
}

//---------------------------------------------------------------------------------------------------
//Iterate point till escape then return the distance estimate
//ToDo: use the closure of the biPoly (fp) for the current d0 because it is constant
//---------------------------------------------------------------------------------------------------
R_lo iteratePtDE( C_lo d0, biPolyClass &fp, refClass &ref, N period, N maxsi, N maxiter, R_lo Bout, N &si ){
	C_lo d(0);
	C_lo dd(0);
	N i(0);
	si = N(0);
	C_lo od0(d0);//Save original delta_c
	d0 -= fp.getShift();
	if(abs(d0)<Bout){
		while(i<maxiter && norm(d)<Bout){//notice that we are comparing d^2 with Bout: norm(d)<Bout <=> d<sqrt(Bout)
			dd = dd * fp.eval_dz(d,d0) + fp.eval_dc(d,d0);
			d = fp.eval(d,d0);
			i+=period;
			si++;
		}
	} //else dd = C_lo(1);
	if(i>maxiter)
		return R_lo(-1);
	//restore original delta_c
	d0 = od0;
	//Compute current d wrt current ref
	d = d - ref[i];
	//Use perturbation
	perturbationClassD p(d0,d,dd,si,i);
	p.run(ref,maxsi,maxiter);
	if(p.haveEscaped())
		return p.getCol();
	else
	    return R_lo(-1);
}

//---------------------------------------------------------------------------------------------------
// iterate using the given list of SSAs.
// d0 is relative to the center of the screen. The SSAs transparently transform the point into their own reference frame when called thanks to their m_shift member variable.
//---------------------------------------------------------------------------------------------------
R_lo iteratePtDE1( C_lo d0, vector<biPolyClass> &SSAs, refClass &ref, N maxsi, N maxiter, N &si /*to be removed*/, int &iters, float &trans){
	//try the SSAs from the latest (deepest) to the first. if not possible use perturbation until we get near to 0 again then repeat.
	//ToDo: edit refClass to add another run(inside radius) function that verify if the iterated point returns near 0
	//ToDo2: we need to test for escapes AFTER the iteration --> edit biPolyClass to compute the "other" bailout.
	
	C_lo d(0);
	C_lo dd(0);
	N i(0);
	si = N(0);
	N nbrSSAs(SSAs.size());
	N curSSA(-1);
	
	//first iteration with SSA. It uses bailout radius in parameter space.
	for(N k = nbrSSAs - 1; k>=0; k--){
		C_lo d00 = d0 - SSAs[k].getShift();//delta_c coordinates have de be wrt current SSA
		if(abs(d00)<SSAs[k].getEscRadius()){
			//----cout << "curSSA = " << curSSA << endl;
			dd = dd * SSAs[k].eval_dz(d,d00) + SSAs[k].eval_dc(d,d00);
			d = SSAs[k].eval(d,d00);
			i+= SSAs[k].getPeriod();
			si++;
			curSSA = k;
			break;
		}
	}
	N firstSSA = curSSA;
	R_lo d_norm(1.0/0.0);
	//Use SSAs from lower to upper to iterate
	for(; curSSA>=0 && SSAs[curSSA].getPeriod() >= 1; curSSA--){
             	C_lo d00 = d0 - SSAs[curSSA].getShift();//delta_c coordinates have de be wrt current SSA
		while(si < maxsi && i<maxiter && norm(d)<SSAs[curSSA].getEscRadius()) {// norm(d)<SSAs[curSSA].getEscRadius() <=> abs(d)<sqrt(SSAs[curSSA].getEscRadius())
			if (curSSA == firstSSA && norm(d) < d_norm)
			{
			  d_norm = norm(d);
			  // check interior: newton iterations for w0 = f^(si*p)(w0, d0)
			  C_lo w0(d);
			  C_lo dw;
			  bool converged = false;//------------------
			  for (int step = 0; step < 16; ++step)
			  {
			    C_lo w(w0);
			    dw = C_lo(1);
			    for (int n = 0; n < si; ++n)
			    {
			      dw = dw * SSAs[curSSA].eval_dz(w, d00);
			      w = SSAs[curSSA].eval(w, d00);
			    }
			    C_lo w0_next = w0 - (w - w0) / (dw - C_lo(1));
			    R_lo delta = norm(w0_next - w0);
			    //C_lo delta = (w0_next - w0);//-----------------
			    w0 = w0_next;
			    R_lo epsilon2(0); // FIXME
			    if (delta <= epsilon2) { converged = true;break; }; // converged
			    //if (abs(delta.re) < abs(w0.re) * R_lo(1e-6) && abs(delta.im) < abs(w0.im) * R_lo(1e-6)) { converged = true; break;}//-----------------------
			  }
			  if (converged && norm(dw) < R_lo(1))
			  {
			    iters = maxiter;
			    trans = 0;
			    return 0;
#if 0			    
			    // is interior, calculate interior DE
			    C_lo z(w0), dz(1), dc(0), dzdz(0), dcdz(0);
			    for (int n = 0; n < si; ++n)
			      up.eval(z, dz, dc, dzdz, dcdz);
			    R_lo de = (R_lo(1) - norm(dz)) / abs(dcdz + (dzdz * dc) / (C_lo(1) - dz));
			    iters = si * period;
			    trans = 0.0f;
			    return -de;
#endif
			  }
			}
			dd = dd * SSAs[curSSA].eval_dz(d,d00) + SSAs[curSSA].eval_dc(d,d00);
			d  = SSAs[curSSA].eval(d,d00);
			i += SSAs[curSSA].getPeriod();
			si++;
		}
		if(si >= maxsi || i >= maxiter) break;
	}
	
	//We are out of SSAs or maxiter attained
	//R_lo f(1);
	//if(i == 0) {dd = C_lo(1); f = R_lo(.5);}
	if(si >= maxsi || i>=maxiter)
	{
	  iters = maxiter;
	  trans = 0;
	  return R_lo(0);
	}
	
	//Use perturbation to finish the job.
	//ToInvestigate: Usually when arriving here (same thing with SA) delta_c (d0 here) is too small wrt d.
	//               One can use perturbation without adding delta_c. Some other optimizations may become possible. (from comment by Pauldelbrot @fractalforums.com)
	d -= ref[i]; //put d in reference orbit's local frame
	perturbationClassD p(d0,d,dd,si,i);//We use original d0 because the reference orbit was computed there.
	p.run(ref,maxsi,maxiter);
	
	if(p.haveEscaped())
	{
	  iters = p.getIters();
	  trans = p.getTrans();
	  return p.getCol();
	}
	else
	{
	  iters = maxiter;
	  trans = 0;
	  return R_lo(0);
	}
}

//---------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------
void plot(image &i,N x, N y, R_lo v, N si)
{
  if(v<0){
	i.plot(x, y, 0, 0, 0);
	return;
  }
  //v=std::sqrt(v);
  N r(N(255.*(.5*sin(0.1*v)+.5) ));
  N g(N(255.*(.5*sin(0.09*v)+.5) ));
  N b(N(255.*(.5*sin(0.12*v)+.5) ));
  //N r((si & 1) * 255);
  si=si;//for when si is not used and we want the compiler to not say warning!
  i.plot(x, y, r, g, b);
}

void plotDE(image &i,N x, N y, R_lo v, R_lo pixelScale)
{
  using std::sqrt;
  if(v<0){
	i.plot(x, y, 128, 0, 0);
	return;
  }
  v = sqrt(v * pixelScale);//std::sqrt(v);
  N r(N(255.*(.5*sin(0.1*v)+.5) ));
  N g(N(255.*(.5*sin(0.09*v)+.5) ));
  N b(N(255.*(.5*sin(0.12*v)+.5) ));
  i.plot(x, y, r, g, b);
}
//-------------------------------------------------------------------------------------------------------------
// entry point

static uint32_t burtle_hash(uint32_t a)
{
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

// uniform in [0,1)
static double jitter(uint32_t x, uint32_t y, uint32_t c)
{
  return burtle_hash(x + burtle_hash(y + burtle_hash(c))) / (double) (0x100000000LL);
}

void main1(const C_hi &c, N bm, N bn, N period, N maxsi, N maxiters, N width, N height, float *buffer, R_lo r0)
{
  R_lo tmax = r0;

  //Compute bivariate polynomial approximating period iterations--------------------------
  //ToDo: compute also the bivariate polynomial for the "upper level" hyperbolic components centres (HCC):
  //   - Use the running bivariate polynomial... actually the part that doesn't depend on "u" (the SA part)
  //   - Find the upper HCCs by solving the zroes of the SA poly when applicable.
  //   - We will not suppose that "c" is at the center of a minibrot anymore.
  //   - The last (deepest) HC is the one which center is inside or near the rendered area
  //   This procedure may fail when the SA part of tha bipoly is no longer accurate while we haven't found
  //  an HCC inside the rendered area and that area is too small to be handled by the previous HCC
  //  (insufficient accuracy). Maybe a kind of "super" perturbation?
  //    For now we will suppose we have a good deepest HCC.  
  //ToDo: what about the case where no deepest minibrot is found... We still need good reference points... maybe subdivide the rendered area?
  biPolyClass fp(bm,bn);
  vector<biPolyClass> SSAs;//to store the set of the relevent hyperbolic components' SSAs from lowest to highest period.
  R_lo smallest_ref_rad_so_far(1000);//for atom domain
  refClass ref;
  C_hi z(0);
  C_lo zlo(0);
  bool bNotFinished = true;
  bool stopRecSSAs = false;

  // claude 2020-01-31
  C_lo HCcentre_old(0);
  int bits_required = 0;
  
  for(N i = 0; i < period && bNotFinished; i++){
	ref.adde(zlo);
	z = z*z + c;
	zlo = c_lo(z);
	fp.cstep(zlo);
	//See if we have a new atom domain... Atom domain does work but not ver well
	//R_lo eRad = fp.getRadius() * R_lo(1000.);//scale down in order to not choose too far HC center. ToVerify... worse!
	//doing "if(zmag < /*eRad*/ R_lo(.5)*smallest_ref_rad_so_far)" is better (with the .5 factor which removes a lot of SSAs)
	R_lo zmag = abs(zlo);
	
	if(zmag < /*eRad*/ R_lo(.5)*smallest_ref_rad_so_far /*&& i>7*/ && !stopRecSSAs){
		//compute the SSA for current hyperbolic component and store it in SSAs:		
		
		//get the SA
		polyClass SA = fp.getSAPoly();
		
		//compute the corresponding HC center
		C_lo HCcentre = SA.getRoot();//HCcentre is relative to c.

#if 0
		// compute escape radius
		C_hi wc(c + C_hi(HCcentre));
		C_hi wz(0);
		C_lo wdz(1);
		for (int j = 1; j < i + 1; ++j)
		{
			wz = wz * wz + wc;
			C_lo wzlo(c_lo(wz));
			wdz = R_lo(2) * wzlo * wdz;
		}
		R_lo escRadius = R_lo(2) / abs(wdz);
#endif
		
		//get shifted SSA
		biPolyClass SSA = fp.getShiftedBP(HCcentre);
		SSA.setPeriod(i+1);
		SSA.setEscRadius();
		
		if(SSA.getEscRadius() > R_lo(0.1) * tmax){
			//Store the shifted SSA into SSAs
			//We don't need HC with too low period. They may be slower than using perturbation.
			if(i>7)//---------------------------------------------- 
				SSAs.push_back(SSA);
		
			//if HCcentre is inside or near the rendered area we should stop!
			//--> We will consider that HCcenter is near when it's magnitude is less than say 1000th the rendered area width? to study!!!
			//if(abs(HCcentre) < R_lo(100) * tmax) bNotFinished = false;
		
			//Print infos:
			cout << "Atom domain: period: " << i+1 << endl;
			cout << "\t zmag:      " << zmag << endl;
			cout << "\t HC centre: " << HCcentre << "\t (relative to location)" << endl;
			cout << "\t Esc R(new):" << SSA.getEscRadius() << endl;
			cout << "\t Esc R(old):" << SSA.getRadius() << endl;

			// claude 2020-01-31
			int bits = int(ceil(-log2(min(R_lo(2), SSA.getEscRadius()) / (width * abs(HCcentre - HCcentre_old)))));
			bits_required = max(bits_required, bits);
			HCcentre_old = HCcentre;
			cout << "\t Precision: " << bits << endl;

			//fp.print();
			cout << "-----------------------------------------------" << endl;
		} else stopRecSSAs = true;
		//
		smallest_ref_rad_so_far = zmag;
	}
  }
  cerr << "__" << c_lo(z) << endl;
  fp.print();
  cerr << "R == " << fp.getRadius() << endl;

  // claude 2020-01-31
  if (bits_required > R_lo_mantissa_precision_bits)
  {
    cerr << "WARNING: insufficient precision:  " << bits_required << " >  " << R_lo_mantissa_precision_bits << endl;
  }
  else
  {
    cerr << "NOTICE:  precision is sufficient: " << bits_required << " <= " << R_lo_mantissa_precision_bits << endl;
  }

  //return 1;
  //--------------------------------------------------------------------------------------
  //Compute picture
  //--------------------------------------------------------------------------------------
  
  //N nSSA(SSAs.size()-2);
  //cout << "-------------------------------\nCurrent nucleus: period: " << SSAs[nSSA].getPeriod() << endl;
  //SSAs[nSSA].print();
  N progress = 0;
  #pragma omp parallel for schedule(dynamic)
  for (N y = 0; y < height; ++y)
  {
    for (N x = 0; x < width; ++x)
    {
      R_lo t0 = (x + jitter(x, y, 0)) / width * 2 * pi;
      R_lo c0 = cos(t0);
      R_lo s0 = sin(t0);
      R_lo t1 = (x + jitter(x, y, 0) + 1) / width * 2 * pi;
      R_lo c1 = cos(t1);
      R_lo s1 = sin(t1);
      R_lo l0 = (y + jitter(x, y, 1)) / width * 2 * pi;
      R_lo r0 = exp(8 - l0);
      C_lo dc(r0 * c0, r0 * s0);
      C_lo dc1(r0 * c1, r0 * s1);
      R_lo pixel_spacing = sqrt(norm(dc1 - dc));
	  N si(0);
	  int iters = 0;
	  float trans = 0.0f;
	  //we need to send the nucleus position wrt the center of the picture.
	  R_lo v = iteratePtDE1( dc, SSAs, ref, maxsi, maxiters, si, iters, trans);//
	  //R_lo v = iteratePtDE( dc, fp, ref, period, maxiters, Bout, si, iters, trans);

    float dwell = v <= 0 ? -1 : 16 + iters + trans;
    float distance = float(abs(v / pixel_spacing));
    float angle = 0;
    float isperiod = 0;
    float isnewton = 0;
    float *result = buffer + size_t(y) * width * CHANNELS + x * CHANNELS;
    result[0] = dwell;
    result[1] = distance;
    result[2] = angle;
    result[3] = isperiod;
    result[4] = isnewton;

    }
    #pragma omp critical
    cerr << "\timage scanline " << ++progress << " / " << height << "\r";
  }
}
#endif

#ifdef NANOMB1
// m.cpp 2018 Knighty
// based on some code by Claude Heiland-Allen
// LICENSE: public domain, cc0, whatever
// acceleration method for rendering Mandelbrot set
// based on approximation of n iterations around minibrot of period n using bivariate polynomial
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Knighty (2018-07-10): Edited the code in order to make it possible to render locations where the
//                       center is not exactly on the nucleus:
//                       - added the tmpPolyClass: used to find the position if the nucleus.
//                       - added code to compute the reference orbit wrt the nucleus.
//                       - added iteratePtDE2() which is almost exactly the same as iteratePtDE() but
//                         designed to take into account the new reference orbit.
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// compile:
//  g++ -std=c++11 -Ofast -mfpmath=387 -march=native mb.cpp -lmpfr -lgmp -Wall -Wextra -pedantic -fopenmp
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// run:
//  ./a --help
// view results:
//  display out.ppm
//#undef RC_INVOKED
//#undef __STRICT_ANSI__
//#include <float.h>

//#include <cassert>
#include <cstdlib>
#include <cstring>

#include <limits>

#include <iostream>
#include <fstream>

#if 0
#include <complex>
#define COMPLEX complex
#else
//A little problem with mpreal which includes <complex> so I had to change the name of the custom complex class to fcomplex
#include "complex.h"
#define COMPLEX fcomplex
#endif

#include <vector>

using namespace std;

// type aliases
typedef int N;
typedef mp_real R_hi;
typedef COMPLEX<R_hi> C_hi;

//--------------------------------------------------------------------------------------------------------
// Conversion routines

inline double r_lo(const R_hi &z, const double &dummy)
{
  (void) dummy;
  return mpfr_get_ld(z.m, MPFR_RNDN);
}

inline long double r_lo(const R_hi &z, const long double &dummy)
{
  (void) dummy;
  return mpfr_get_ld(z.m, MPFR_RNDN);
}

template<typename R>
inline R r_lo(const char *s, const R &dummy)
{
  return r_lo(R_hi(s), dummy);
}

template <typename R>
inline COMPLEX<R> c_lo(const C_hi &z, const R &dummy)
{
  return COMPLEX<R>(r_lo(real(z), dummy), r_lo(imag(z), dummy));
}

//--------------------------------------------------------------------------------------------------------
// simple RGB24 image
class image
{
public:
  N width;
  N height;
  vector<uint8_t> rgb;

  // construct empty image
  image(N width, N height)
  : width(width)
  , height(height)
  , rgb(width * height * 3)
  { };

  // plot a point
  void plot(N x, N y, N r, N g, N b)
  {
    N k = (y * width + x) * 3;
    rgb[k++] = r;
    rgb[k++] = g;
    rgb[k++] = b;
  };

  // save to PPM format
  void save(const char *filename)
  {
    FILE *out = fopen(filename, "wb");
    fprintf(out, "P6\n%d %d\n255\n", width, height);
    fwrite(&rgb[0], width * height * 3, 1, out);
    fflush(out);
    fclose(out);
  }
};
//--------------------------------------------------------------------------------------------------------
template <typename R_lo>
class refClass{
	typedef COMPLEX<R_lo> C_lo;
	N m_n;
	vector<C_lo> m_v;
public:
	refClass(): m_n(0), m_v(0) {}
	void adde(C_lo c){ m_v.push_back(c); m_n++;}
	const C_lo & operator[](N i) const { return m_v[i % m_n];}
};

template <typename R_lo>
class perturbationClass{
	typedef COMPLEX<R_lo> C_lo;
	C_lo m_d0;
	C_lo m_d;
	N    m_n0;
	R_lo m_col;
	bool m_escaped;
public:
	perturbationClass(C_lo d0, C_lo d, N n0): m_d0(d0), m_d(d), m_n0(n0), m_col(0), m_escaped(false) {}
	void run(const refClass<R_lo> &ref, N maxiter){
		for(N i=m_n0; i < maxiter; i++){
			C_lo zn(ref[i]);
			m_d = m_d * (R_lo(2) * zn + m_d) + m_d0;
			R_lo r(norm(zn + m_d));
			if (r > R_lo(256)){
				m_escaped = true;
				m_col = R_lo(i+1) - R_lo(std::log(std::log(double(r)))/std::log(2.0));
				return;
			}
		}
	}
	R_lo getCol(){return m_col;}
	bool haveEscaped(){return m_escaped;}
};


template <typename R_lo>
class perturbationClassD{
	typedef COMPLEX<R_lo> C_lo;
	C_lo m_d0;
	C_lo m_d;
	C_lo m_dd;
	N    m_n0;
	R_lo m_col;
	bool m_escaped;
	int   m_iters;
	float m_trans;
public:
	perturbationClassD(C_lo d0, C_lo d, C_lo dd, N n0): m_d0(d0), m_d(d), m_dd(dd), m_n0(n0), m_col(0), m_escaped(false) {}
	void run(const refClass<R_lo> &ref, N maxiter){
		for(N i=m_n0; i < maxiter; i++){
			C_lo zn(ref[i]);
			m_dd = R_lo(2) * m_dd * (m_d + zn) + R_lo(1);
			m_d = m_d * (R_lo(2) * zn + m_d) + m_d0;
			R_lo r(norm(zn + m_d));
			if (r > R_lo(1e10)){
				m_escaped = true;
				double j = i+1 - log(log(double(r)))/log(2.0);
				m_iters = floor(j);
				m_trans = j - m_iters;
				m_col = R_lo(2 * std::sqrt(double(r)) * std::log(double(r))) / abs(m_dd);
				return;
			}
		}
	}
	int   getIters(){return m_iters;}
	float getTrans(){return m_trans;}
	R_lo  getCol(){return m_col;}
	bool haveEscaped(){return m_escaped;}
};

template <typename R_lo> class uniPolyClass;
template <typename R_lo> class tmpPolyClass;
template <typename R_lo>
class biPolyClass {
	friend class uniPolyClass<R_lo>;
	friend class tmpPolyClass<R_lo>;
	typedef COMPLEX<R_lo> C_lo;
	N m_m, m_n;
	C_lo tab[128][128];
	C_lo ttab[128][128];
	void mcopy(){
		for(N l=0; l <= m_m; l++)
			for(N c=0; c<= m_n; c++)
				ttab[l][c] = tab[l][c];
	}
	C_lo csqrc(N k, N l){
		C_lo v(0);
		for(N i=0; i <= k; i++)
			for(N j=0; j <= l; j++)
				v += ttab[i][j] * ttab[k-i][l-j];
		return v;
	}
public:
	biPolyClass(N m, N n): m_m(m), m_n(n) {
		for(N l=0; l <= m_m; l++)
			for(N c=0; c<= m_n; c++)
				tab[l][c] = C_lo(0);
		tab[1][0] = C_lo(1);
	}

	void sqr(){
		mcopy();
		for(N i=0; i <= m_m; i++)
			for(N j=0; j <= m_n; j++)
				tab[i][j] = csqrc(i,j);
	}

	void cstep(C_lo z){
		sqr();
		tab[0][0]  = z;
		tab[0][1] += C_lo(1);
	}

	C_lo eval(C_lo u, C_lo v){
		C_lo r(0);
		C_lo ui(1);
		for(N i=0; i <= m_m; i+=2){
			C_lo vj(ui);
			for(N j=0; j <= m_n; j++){
				r += tab[i][j] * vj;
				vj *= v;
			}
			ui *= u*u;
		}
		return r;
	}

	C_lo eval_dc(C_lo u, C_lo v){
		C_lo r(0);
		C_lo ui(1);
		for(N i=0; i <= m_m; i+=2){
			C_lo vj(ui);
			for(N j=1; j <= m_n; j++){
				r += R_lo(j) * tab[i][j] * vj;
				vj *= v;
			}
			ui *= u*u;
		}
		return r;
	}

	C_lo eval_dz(C_lo u, C_lo v){
		C_lo r(0);
		C_lo ui(u);
		for(N i=2; i <= m_m; i+=2){
			C_lo vj(C_lo(i) * ui);
			for(N j=0; j <= m_n; j++){
				r += tab[i][j] * vj;
				vj *= v;
			}
			ui *= u*u;
		}
		return r;
	}

	void print(){
		for(N i=0; i <= m_m; i++){
			for(N j=0; j <= m_n; j++){
				cout << "i: " << i << "\tj: " << j << "\tval: " << tab[i][j] << endl;
			}
			cout << "-----------------------" << endl;
		}
	}

	R_lo getRadius(){
		//return abs(tab[0][1])/abs(tab[0][2]);
		R_lo r(0);
		for(int i = 0; i < 10; i++){
			C_lo den(0);
			R_lo rr(1);
			for(int j = 2; j <=m_n; j++){
				den += tab[0][j] * rr;
				rr  *= r;
			}
			r = abs(tab[0][1])/abs(den);
		}
		return R_lo(0.5) * r;
	}
};

//temporary poly class for solving for nucleus relative position. It is initialized with the part of the SSA that depends only on c.
template <typename R_lo>
class tmpPolyClass {
	typedef COMPLEX<R_lo> C_lo;
	N m_m;
	C_lo b[128];
public:
	tmpPolyClass(const biPolyClass<R_lo> &p): m_m(p.m_n) {
		for(N i = 0; i <= m_m; i++)
			b[i] = p.tab[0][i];
	}
	
	//evaluation function. It would be nice to add an ()-operator... or not. :)
	C_lo eval(C_lo u){
		C_lo r(0);
		C_lo ui(1);
		for(N i=0; i <= m_m; i++){
			r += b[i] * ui;
			ui *= u;
		}
		return r;
	}
	
	//evaluate derivative.
	C_lo evalD(C_lo u){
		C_lo r(0);
		C_lo ui(1);
		for(N i=1; i <= m_m; i++){
			r += R_lo(i) * b[i] * ui;
			ui *= u;
		}
		return r;
	}
	
	//Gives the nearest root to the 0. To use if and when applicable (that is the reference is near 0... atom domain thing!)
	//Newton should do the job (otherwise IA-Newton ?).
	C_lo getRoot(){
		C_lo rt(0);
		//R_lo t = abs(eval(rt));
		for(N i=0; i<30; i++){
			C_lo num = eval(rt);
			C_lo den = evalD(rt);
			C_lo delta = num / den;
			num = rt;
			rt -= delta;
			if( rt.re == num.re && rt.im == num.im) break;
		}
		return rt;
	}
};

// https://fractalforums.org/f/28/t/277/msg7952#msg7952
template <typename R_lo>
class uniPolyClass {
	typedef COMPLEX<R_lo> C_lo;
	N m_m;
	C_lo b[128];
	C_lo dbdc[128];
public:
	uniPolyClass(const biPolyClass<R_lo> &p, C_lo c): m_m(p.m_m) {
		for(N i = 0; i <= m_m; i += 2)
		{
		  C_lo s(0), ds(0), cj(1), cj1(0);
		  for (N j = 0; j <= p.m_n; ++j)
		  {
		    s += p.tab[i][j] * cj;
		    ds += C_lo(j) * p.tab[i][j] * cj1;
		    cj *= c;
		    cj1 *= c;
		    if (j == 0) cj1 = C_lo(1);
		  }
		  b[i] = s;
		  dbdc[i] = ds;
		}
	}

	void eval(C_lo &z) const {
	  C_lo zs(0), zi(1);
	  for (N i = 0; i <= m_m; i += 2)
	  {
	    zs += b[i] * zi;
	    zi *= z * z;
	  }
	  z = zs;
	}

	void eval(C_lo &z, C_lo &dc) const {
	  C_lo zs(0), dcs(0), zi(1), zi1(0);
	  for (N i = 0; i <= m_m; i += 2)
	  {
	    dcs += C_lo(i) * b[i] * zi1 * dc + dbdc[i] * zi;
	    zs += b[i] * zi;
	    zi *= z * z;
	    zi1 *= z * z;
	    if (i == 0) zi1 = z;
	  }
	  z = zs;
	  dc = dcs;
	}

	void eval_dz(C_lo &z, C_lo &dz) const {
	  C_lo zs(0), dzs(0), zi(1), zi1(0);
	  for (N i = 0; i <= m_m; i += 2)
	  {
	    dzs += C_lo(i) * b[i] * zi1 * dz;
	    zs += b[i] * zi;
	    zi *= z * z;
	    zi1 *= z * z;
	    if (i == 0) zi1 = z;
	  }
	  z = zs;
	  dz = dzs;
	}

	void eval(C_lo &z, C_lo &dz, C_lo &dc, C_lo &dzdz, C_lo &dcdz) const {
	  C_lo zs(0), dzs(0), dcs(0), dzdzs(0), dcdzs(0), zi(1), zi1(0), zi2(0);
	  for (N i = 0; i <= m_m; i += 2)
	  {
	    dcdzs += C_lo(i) * C_lo(i - 1) * b[i] * zi2 * dz * dc + C_lo(i) * b[i] * zi1 * dcdz + C_lo(i) * dbdc[i] * zi1 * dz;
	    dzdzs += C_lo(i) * C_lo(i - 1) * b[i] * zi2 * dz * dz + C_lo(i) * b[i] * zi1 * dzdz;
	    dcs += C_lo(i) * b[i] * zi1 * dc + dbdc[i] * zi;
	    dzs += C_lo(i) * b[i] * zi1 * dz;
	    zs += b[i] * zi;
	    zi *= z * z;
	    zi1 *= z * z;
	    zi2 *= z * z;
	    if (i == 0) zi1 = z;
	    if (i == 0) zi2 = C_lo(1);
	  }
	  z = zs;
	  dz = dzs;
	  dc = dcs;
	  dzdz = dzdzs;
	  dcdz = dcdzs;
	}
};


template <typename R_lo>
R_lo iteratePt( COMPLEX<R_lo> d0, biPolyClass<R_lo> &fp, refClass<R_lo> &ref, N period, N maxiter, R_lo Bout, N &si ){
	typedef COMPLEX<R_lo> C_lo;
	C_lo d(0);
	C_lo ld(0);
	N i(0);
	si = N(0);
	if(abs(d0)<Bout){
		while(i<maxiter && norm(d)<Bout){
			d = fp.eval(d,d0);
			i+=period;
			si++;
		}
	}
	if(i>maxiter)
		return R_lo(-1);
	perturbationClass<R_lo> p(d0,d,i);
	p.run(ref,maxiter);
	if(p.haveEscaped())
		return p.getCol();
	else
	    return R_lo(-1);
}

template <typename R_lo>
R_lo iteratePtDE( const COMPLEX<R_lo> &d0, const biPolyClass<R_lo> &fp, const refClass<R_lo> &ref, N period, N maxiter, R_lo Bout, N &si, int &iters, float &trans){
	typedef COMPLEX<R_lo> C_lo;
	C_lo d(0);
	C_lo dd(0);
	N i(0);
	si = N(0);
	R_lo d_norm(1.0/0.0);
	if(abs(d0)<Bout){
	  uniPolyClass<R_lo> up(fp, d0);
		while(i<maxiter && norm(d)<Bout){
			up.eval(d, dd);
			i+=period;
			si++;
			if (norm(d) < d_norm)
			{
			  d_norm = norm(d);
			  // check interior: newton iterations for w0 = f^(si*p)(w0, d0)
			  C_lo w0(d);
			  C_lo dw;
			  for (int step = 0; step < 16; ++step)
			  {
			    C_lo w(w0);
			    dw = C_lo(1);
			    for (int n = 0; n < si; ++n)
			    {
			      up.eval_dz(w, dw);
			    }
			    C_lo w0_next = w0 - (w - w0) / (dw - C_lo(1));
			    R_lo delta = norm(w0_next - w0);
			    w0 = w0_next;
			    R_lo epsilon2(0); // FIXME
			    if (delta <= epsilon2) break; // converged
			  }
			  if (norm(dw) < R_lo(1))
			  {
			    // is interior, calculate interior DE
			    C_lo z(w0), dz(1), dc(0), dzdz(0), dcdz(0);
			    for (int n = 0; n < si; ++n)
			      up.eval(z, dz, dc, dzdz, dcdz);
			    R_lo de = (R_lo(1) - norm(dz)) / abs(dcdz + (dzdz * dc) / (C_lo(1) - dz));
			    iters = si * period;
			    trans = 0.0f;
			    return -de;
			  }
			}
		}
	} //else dd = C_lo(1);//knighty: That was a mistake. We begin iterating at 0 so derivative doesn't have to be changed. 
	if(i>=maxiter)
	{
	  iters = maxiter;
	  trans = 0.0f;
	  return R_lo(0);
	}
	perturbationClassD<R_lo> p(d0,d,dd,i);
	p.run(ref,maxiter);
	if(p.haveEscaped())
	{
	  iters = p.getIters();
	  trans = p.getTrans();
	  return p.getCol();
	}
	else
	{
	  iters = maxiter;
	  trans = 0.0f;
	  return R_lo(0);
	}
}

template <typename R_lo>
R_lo iteratePtDE2( const COMPLEX<R_lo> &d0, const biPolyClass<R_lo> &fp, const refClass<R_lo> &ref, COMPLEX<R_lo> nucleusPos, N period, N maxiter, R_lo Bout, N &si, int &iters, float &trans){
	typedef COMPLEX<R_lo> C_lo;
	C_lo d(0);
	C_lo dd(0);
	N i(0);
	si = N(0);
	R_lo d_norm(1.0/0.0);
	if(abs(d0)<Bout){
	  uniPolyClass<R_lo> up(fp, d0);
		while(i<maxiter && norm(d)<Bout){
			up.eval(d, dd);
			i+=period;
			si++;
			if (0)//norm(d) < d_norm)
			{
			  d_norm = norm(d);
			  // check interior: newton iterations for w0 = f^(si*p)(w0, d0)
			  C_lo w0(d);
			  C_lo dw;
			  bool converged = false;//------------------
			  for (int step = 0; step < 16; ++step)
			  {
			    C_lo w(w0);
			    dw = C_lo(1);
			    for (int n = 0; n < si; ++n)
			    {
			      up.eval_dz(w, dw);
			    }
			    C_lo w0_next = w0 - (w - w0) / (dw - C_lo(1));
			    //R_lo delta = norm(w0_next - w0);
				C_lo delta = (w0_next - w0);//-----------------
			    w0 = w0_next;
			    //R_lo epsilon2(0); // FIXME
			    //if (delta <= epsilon2) break; // converged
				if (abs(delta.re) < abs(w0.re) * R_lo(1e-6) && abs(delta.im) < abs(w0.im) * R_lo(1e-6)) { converged = true; break;}//-----------------------
			  }
			  if (converged && norm(dw) < R_lo(1))
			  {
			    // is interior, calculate interior DE
			    C_lo z(w0), dz(1), dc(0), dzdz(0), dcdz(0);
			    for (int n = 0; n < si; ++n)
			      up.eval(z, dz, dc, dzdz, dcdz);
			    R_lo de = (R_lo(1) - norm(dz)) / abs(dcdz + (dzdz * dc) / (C_lo(1) - dz));
			    iters = si * period;
			    trans = 0.0f;
			    return -de;
			  }
			}
		}
	} //else dd = C_lo(1);//knighty: That was a mistake. We begin iterating at 0 so derivative doesn't have to be changed. 
	if(i>=maxiter)
	{
	  iters = maxiter;
	  trans = 0.0f;
	  return R_lo(0);
	}
	//d0 and d have to be transformed into ref frame.
	C_lo d0_(d0 - nucleusPos);
	d  -= ref[i];
	perturbationClassD<R_lo> p(d0_,d,dd,i);
	p.run(ref,maxiter);
	if(p.haveEscaped())
	{
	  iters = p.getIters();
	  trans = p.getTrans();
	  return p.getCol();
	}
	else
	{
	  iters = maxiter;
	  trans = 0.0f;
	  return R_lo(0);
	}
}

void plot(image &i,N x, N y, double v, N si)
{
  (void) si;
  if(v<0){
	i.plot(x, y, 0, 0, 0);
	return;
  }
  //v=std::sqrt(v);
  N r(N(255.*(.5*sin(0.1*v)+.5) ));
  N g(N(255.*(.5*sin(0.09*v)+.5) ));
  N b(N(255.*(.5*sin(0.12*v)+.5) ));
  //N r((si & 1) * 255);
  i.plot(x, y, r, g, b);
}

template <typename R_lo>
void plotDE(image &i,N x, N y, R_lo u, R_lo pixelScale)
{
  double v = 16 * log(abs(double(u * pixelScale))+1e-10);
  //if(u<0) v*=0.01;
  N r(N(255.*(.5*sin(0.1*v)+.5) ));
  N g(N(255.*(.5*sin(0.09*v)+.5) ));
  N b(N(255.*(.5*sin(0.12*v)+.5) ));
  i.plot(x, y, r, g, b);
}

static uint32_t burtle_hash(uint32_t a)
{
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

// uniform in [0,1)
static double jitter(uint32_t x, uint32_t y, uint32_t c)
{
  return burtle_hash(x + burtle_hash(y + burtle_hash(c))) / (double) (0x100000000LL);
}

struct kfb
{
  int w;
  int h;
  int n;
  int **iters;
  float **trans;
  float **de;
  kfb(int _w, int _h, int _n) : w(_w), h(_h), n(_n)
  {
    iters = new int  *[w];
    trans = new float*[w];
    de    = new float*[w];
    for (int x = 0; x < w; ++x)
    {
      iters[x] = new int  [h];
      trans[x] = new float[h];
      de   [x] = new float[h];
    }
  }
  ~kfb()
  {
    for (int x = 0; x < w; ++x)
    {
      delete[] iters[x];
      delete[] trans[x];
      delete[] de   [x];
    }
    delete[] iters;
    delete[] trans;
    delete[] de;
  }
  void plot(int x, int y, int _iters, float _trans, float _de)
  {
    iters[x][y] = _iters;
    trans[x][y] = _trans;
    de   [x][y] = _de;
  }
  void save(const char *filename)
  {
    FILE *f = fopen(filename, "wb");
    fwrite("KFB", 3, 1, f);
    fwrite(&w, sizeof(w), 1, f);
    fwrite(&h, sizeof(h), 1, f);
    for (int x = 0; x < w; ++x) fwrite(iters[x], sizeof(iters[x][0]) * h, 1, f);
    int iterdiv = 1;
    fwrite(&iterdiv, sizeof(iterdiv), 1, f);
    int colours = 2;
    fwrite(&colours, sizeof(colours), 1, f);
    unsigned char keys[] = { 0, 0, 0, 255, 255, 255 };
    fwrite(keys, sizeof(keys), 1, f);
    fwrite(&n, sizeof(n), 1, f);
    for (int x = 0; x < w; ++x) fwrite(trans[x], sizeof(trans[x][0]) * h, 1, f);
    for (int x = 0; x < w; ++x) fwrite(de   [x], sizeof(de   [x][0]) * h, 1, f);
    fclose(f);
  }
};

//-------------------------------------------------------------------------------------------------------------
// entry point
template <typename R_lo>
void main1(const C_hi &c, N bm, N bn, N period, N maxiters, N width, N height, float *buffer, const R_lo &tmax)
{
  typedef COMPLEX<R_lo> C_lo;
  //Compute bivariate polynomial approximating period iterations--------------------------
  biPolyClass<R_lo> fp(bm,bn);
  refClass<R_lo> ref;
  C_hi z(0);
  C_lo zlo(0);
  int64_t lastpc = -1;
  for(N i = 0; i < period; i++){
	ref.adde(zlo);
	z = z*z + c;
	zlo = c_lo(z, tmax);
	fp.cstep(zlo);
	int64_t pc = ((i + 1) * 100) / period;
	if (pc > lastpc)
	{
	  cerr << "\rreference " << pc << "%";
	  lastpc = pc;
	}
  }
  cerr << "__" << c_lo(z, tmax) << endl;
  fp.print();
  cerr << "R == " << fp.getRadius() << endl;
  R_lo Bout = fp.getRadius();
  
  //return 1;
  
  //--------------------------------------------------------------------------------------
  //In case the location is not "exactly" at a nucleus, we need to "correct" the perturbation reference.
  //This is because the reference orbit is computed for only one period. If the reference c is outside the mbset, it will eventually escape or be likely off 0.
  //Find the nucleus of the minibrot.
  tmpPolyClass<R_lo> tp(fp);
  C_lo nucleusPos = tp.getRoot();
  //cout << "----------------" << endl << "nucleus rel pos: " << nucleusPos << endl;
  //Rebase the reference orbit to the nucleus
  refClass<R_lo> ref1;
  C_lo zlo1(0);
  for(N i = 0; i < period; i++){
	zlo = ref[i];
	ref1.adde(zlo+zlo1);
	zlo1 = zlo1 * (zlo1 + R_lo(2) * zlo) + nucleusPos;
  }
  //At this point zlo+zlo1 should be very close to 0
  //zlo = c_lo(z, tmax);
  //cout << "----------------" << endl << "at iteration: " << period << ", zlo1 = " << zlo1 << endl;
  //cout << "zlo1+zlo = " << zlo1+zlo << endl;
  //--------------------------------------------------------------------------------------
  N progress = 0;
  #pragma omp parallel for schedule(dynamic)
  for (N y = 0; y < height; ++y)
  {
    for (N x = 0; x < width; ++x)
    {
      R_lo t0 = (x + jitter(x, y, 0)) / width * 2 * pi;
      R_lo c0 = cosl(t0);
      R_lo s0 = sinl(t0);
      R_lo t1 = (x + jitter(x, y, 0) + 1) / width * 2 * pi;
      R_lo c1 = cosl(t1);
      R_lo s1 = sinl(t1);
      R_lo l0 = (y + jitter(x, y, 1)) / width * 2 * pi;
      R_lo r0 = expl(8 - l0);
      C_lo dc(r0 * c0, r0 * s0);
      C_lo dc1(r0 * c1, r0 * s1);
      R_lo pixel_spacing = sqrtl(norm(dc1 - dc));
	  N si(0);
	  int iters = 0;
	  float trans = 0.0f;
	  //we need to send the nucleus position wrt the center of the picture.
	  R_lo v = iteratePtDE2( dc, fp, ref1, nucleusPos, period, maxiters, Bout, si, iters, trans);
	  //R_lo v = iteratePtDE( dc, fp, ref, period, maxiters, Bout, si, iters, trans);

    float dwell = v <= 0 ? -1 : 16 + iters + trans;
    float distance = fabsl(v / pixel_spacing);
    float angle = 0;
    float isperiod = 0;
    float isnewton = 0;
    float *result = buffer + size_t(y) * width * CHANNELS + x * CHANNELS;
    result[0] = dwell;
    result[1] = distance;
    result[2] = angle;
    result[3] = isperiod;
    result[4] = isnewton;

    }
    #pragma omp critical
    cerr << "\rimage scanline " << ++progress << " / " << height;
  }
  cerr << endl;
}
#endif

int main(int argc, char **argv) {
  if (argc != 8) exit(1);
  FILE *out = fopen(argv[1], "wb");
  N w = atoi(argv[2]);
  long double r0 = strtold(argv[3], 0);
  N bits = fmaxl(53, ceill(53 - log2l(r0)));
  mp_real cx(argv[4], bits);
  mp_real cy(argv[5], bits);
  N period = atoi(argv[6]);
  N maxsi = atoi(argv[7]);
  N maxiters = period * maxsi;
  C_hi c(cx, cy);
  N bm = 16;
  N bn = 8;
  N h = roundl(w * (16 - logl(r0)) / (2 * pi));
  size_t bytes = sizeof(float) * w * h * CHANNELS;
  float *buffer = (float *) malloc(bytes);
  main1(c, bm, bn, period, maxsi, maxiters, w, h, buffer, r0);
  fwrite(buffer, bytes, 1, out);
  fflush(out);
  fclose(out);
  return 0;
}
