#ifndef MP_REAL_H
#define MP_REAL_H 1

#include <algorithm>
#include <cmath>
#include <qd/dd_real.h>
#include <qd/qd_real.h>
#include <mpfr.h>
//#define MPFR_RNDN GMP_RNDN

using namespace std;

class mp_real;
inline mp_real operator-(mp_real const &x, double const &y);

class mp_real {
public:
  mpfr_t m;
  mp_real() {
    mpfr_init2(m, 53);
    mpfr_set_d(m, 0, MPFR_RNDN);
  }
  mp_real(const double &s) {
    mpfr_init2(m, 53);
    mpfr_set_d(m, s, MPFR_RNDN);
  }
  mp_real(const long double &s) {
    mpfr_init2(m, 64);
    mpfr_set_ld(m, s, MPFR_RNDN);
  }
  mp_real(const int32_t &s) {
    mpfr_init2(m, 53);
    mpfr_set_si(m, s, MPFR_RNDN);
  }
  mp_real(const mp_real &s) {
    mpfr_init2(m, mpfr_get_prec(s.m));
    mpfr_set(m, s.m, MPFR_RNDN);
  }
  mp_real(mpfr_prec_t p) {
    mpfr_init2(m, p);
  }
  mp_real(mpfr_t s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set(m, s, MPFR_RNDN);
  }
  mp_real(const char *s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set_str(m, s, 10, MPFR_RNDN);
  }
  mp_real(double s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set_d(m, s, MPFR_RNDN);
  }
  mp_real(const mp_real &s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set(m, s.m, MPFR_RNDN);
  }
  mp_real(dd_real s) {
    mp_real a(s.x[0], 108);
    mp_real b(s.x[1], 108);
    mpfr_init2(m, 108);
    mpfr_add(m, a.m, b.m, MPFR_RNDN);
  }
  mp_real(qd_real s) {
    mp_real a(s[0], 216);
    mp_real b(s[1], 216);
    mp_real c(s[2], 216);
    mp_real d(s[3], 216);
    mpfr_init2(m, 216);
    mpfr_add(m, a.m, b.m, MPFR_RNDN);
    mpfr_add(m,   m, c.m, MPFR_RNDN);
    mpfr_add(m,   m, d.m, MPFR_RNDN);
  }

  ~mp_real() {
    mpfr_clear(m);
  }
  operator float() const {
    return mpfr_get_d(m, MPFR_RNDN);
  }
  operator double() const {
    return mpfr_get_d(m, MPFR_RNDN);
  }
  operator long double() const {
    return mpfr_get_ld(m, MPFR_RNDN);
  }
  operator dd_real() const {
    double  a = *this;
    mp_real n = *this - a;
    double  b = n;
    return  dd_real(a, b);
  }
  operator qd_real() const {
    double  a = *this;
    mp_real n = *this - a;
    double  b = n;
    mp_real o = n - b;
    double  c = o;
    mp_real p = o - c;
    double  d = p;
    return  qd_real(a, b, c, d);
  }
#define maxprec(x, y) max(mpfr_get_prec(x.m), mpfr_get_prec(y.m))
  inline mp_real& operator=(mp_real const &y) {
    mpfr_set_prec(m, maxprec((*this), y));
    mpfr_set(m, y.m, MPFR_RNDN);
    return *this;
  }
};

inline bool operator<(mp_real const &x, mp_real const &y) {
  return mpfr_less_p(x.m, y.m);
}

inline mp_real operator+(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_add(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real operator+(mp_real const &x, double const &y) {
  mp_real r(max(mpfr_get_prec(x.m), mpfr_prec_t(53)));
  mpfr_add_d(r.m, x.m, y, MPFR_RNDN);
  return r;
}

inline mp_real operator-(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_sub(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real operator-(mp_real const &x, double const &y) {
  mp_real r(max(mpfr_get_prec(x.m), mpfr_prec_t(53)));
  mpfr_sub_d(r.m, x.m, y, MPFR_RNDN);
  return r;
}

inline mp_real operator*(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_mul(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real operator*(double const &x, mp_real const &y) {
  mp_real r(max(mpfr_prec_t(53), mpfr_get_prec(y.m)));
  mpfr_mul_d(r.m, y.m, x, MPFR_RNDN);
  return r;
}

inline mp_real operator*(mp_real const &x, double const &y) {
  mp_real r(max(mpfr_get_prec(x.m), mpfr_prec_t(53)));
  mpfr_mul_d(r.m, x.m, y, MPFR_RNDN);
  return r;
}

inline mp_real operator/(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_div(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real sqr(mp_real const &x) {
  mp_real r(mpfr_get_prec(x.m));
  mpfr_sqr(r.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real sqrt(mp_real const &x) {
  mp_real r(mpfr_get_prec(x.m));
  mpfr_sqrt(r.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real log(mp_real const &x) {
  mp_real r(mpfr_get_prec(x.m));
  mpfr_log(r.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real atan2(mp_real const &y, mp_real const &x) {
  mp_real r(maxprec(x, y));
  mpfr_atan2(r.m, y.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real pow(mp_real const &y, mp_real const &x) {
  mp_real r(maxprec(x, y));
  mpfr_pow(r.m, y.m, x.m, MPFR_RNDN);
  return r;
}

inline float to_float(mp_real const &x) {
  return mpfr_get_d(x.m, MPFR_RNDN);
}

#undef maxprec

#endif
