#
#    emndl -- exponentially transformed Mandelbrot Set renderer
#    Copyright (C) 2011,2018  Claude Heiland-Allen <claude@mathr.co.uk>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# standard paths/tools
prefix ?= $(HOME)/opt
exec_prefix = $(prefix)
data_prefix = $(prefix)
bindir = $(exec_prefix)/bin
datadir = $(data_prefix)/share/emndl
INSTALL = install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = $(INSTALL) -m 644

# tools
CC = gcc
CXX = g++
GHC = ghc

# flags
OFLAGS = -O3 -march=native -Wall -pedantic -Wextra -Wno-unused-parameter -D_FILE_OFFSET_BITS=64 -pthread -fopenmp
CFLAGS = -std=c99
CXXFLAGS =
GHCFLAGS = -O3 -Wall -threaded -rtsopts -fspec-constr-count=50
LIBS = -lm -lqd -lmpfr -lgmp
EXES = emndl_automu emndl_colourize emndl_downscale emndl_equalize emndl_finalize emndl_calculate emndl_parse emndl_ppmtoy4m emndl_pretty emndl_randomize emndl_unwarp
OBJS = emndl_autotune emndl_autotune.hi emndl_autotune.o Complex.hi Complex.o GridScan.hi GridScan.o MuAtom.hi MuAtom.o Number.hi Number.o Roots.hi Roots.o
DATAS = emndl_audio.pd emndl_comb~.pd emndl_compress~.pd emndl_filter.pd emndl_loader.pd

# build
all: $(EXES)

# clean
clean:
	-rm -f $(EXES) $(OBJS)

# install
install:
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(MAKE) install-bin
	$(MAKE) install-sh
	$(MAKE) install-data

install-strip:
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(MAKE) INSTALL_PROGRAM='$(INSTALL_PROGRAM) -s' install-bin
	$(MAKE) install-sh
	$(MAKE) install-data

install-bin:
	$(INSTALL_PROGRAM) $(EXES) $(DESTDIR)$(bindir)

install-sh:
	$(INSTALL_PROGRAM) emndl.sh $(DESTDIR)$(bindir)

install-data:
	$(INSTALL) -d $(DESTDIR)$(datadir)
	$(INSTALL_DATA) $(DATAS) $(DESTDIR)$(datadir)

# Haskell executables
emndl_autotune: emndl_autotune.hs Complex.hs GridScan.hs MuAtom.hs Number.hs Roots.hs
	$(GHC) $(GHCFLAGS) --make emndl_autotune.hs

# C executables
%: %.c
	$(CC) $(CFLAGS) $(OFLAGS) -o $@ $< $(LIBS)

# C++ executables
%: %.cc
	$(CXX) $(CXXFLAGS) $(OFLAGS) -o $@ $< $(LIBS)

# dependencies
emndl_calculate: emndl_calculate.cc mp_real.h

# meta
.SUFFIXES:
.PHONY: all clean install install-strip install-bin install-sh
