/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct image {
  size_t width, height, channels, levels;
  unsigned char **data;
};

int ilog2(int i) {
  int l = 0;
  while (i > 0) {
    i >>= 1;
    l += 1;
  }
  return l - 1;
}

struct image *loadppms(char *filestem) {
  struct image *img = calloc(sizeof(struct image), 1);
  img->levels = 1;
  for (int i = 0; i < img->levels; ++i) {
    char filename[1024];
    snprintf(filename, 1000, "%s.%02d.ppm", filestem, i);
    FILE *f = fopen(filename, "rb");
    if (! f) exit(6);
    int w, h;
    if (2 != fscanf(f, "P6\n%d %d 255", &w, &h)) exit(4);
    fgetc(f);
    if (i == 0) {
      img->width = w;
      img->height = h;
      img->channels = 3;
      img->levels = ilog2(w) + 1;
      img->data = calloc(sizeof(unsigned char *), img->levels);
      if (! img->data) exit(7);
    } else {
      if (w != img->width  >> i) exit(8);
      if (h != img->height >> i) exit(9);
    }
    img->data[i] = malloc(((size_t)w) * h * 3);
    if (! img->data[i]) exit(11);
    if (1 != fread(img->data[i], ((size_t)w) * h * 3, 1, f)) exit(5);
    fclose(f);
  }
  return img;
}

inline float pixel(const struct image *img, int l, int y, int x, int c) {
  if (0 <= l && l < img->levels
   && 0 <= y && y < img->height >> l
   && 0 <= x && x < img->width  >> l
   && 0 <= c && c < img->channels) {
    return img->data[l][img->channels * ((img->width >> l) * y + x) + c]/255.0f;
  } else {
    exit(42);
  }
}

inline float clamp(float x, float lo, float hi) {
  if (! (x > lo)) return lo;
  if (! (x < hi)) return hi;
  return x;
}

inline float wrap(float x, float lo, float hi) {
  if (x <  lo) return wrap(x + (hi - lo), lo, hi);
  if (x >= hi) return wrap(x - (hi - lo), lo, hi);
  return x;
}

inline float linear(float u, float v, float t) {
  return u * (1 - t) + t * v;
}

inline float pixel2ly(const struct image *img, int l, int y, float x, int c) {
  int x0 = floor(x);
  float xt = x - x0;
  int x1 = x0 + 1;
  x0 = wrap(x0, 0, img->width >> l);
  x1 = wrap(x1, 0, img->width >> l);
  return linear(pixel(img, l, y, x0, c), pixel(img, l, y, x1, c), xt);
}

inline float pixel2l(const struct image *img, int l, float y, float x, int c) {
  int y0 = floor(y);
  float yt = y - y0;
  int y1 = y0 + 1;
  y0 = clamp(y0, 0, (img->height >> l) - 1);
  y1 = clamp(y1, 0, (img->height >> l) - 1);
  return linear(pixel2ly(img, l, y0, x, c), pixel2ly(img, l, y1, x, c), yt);
}

inline float pixel2(const struct image *img, float l, float y, float x, int c) {
  int l0 = floor(l);
  float lt = l - l0;
  int l1 = l0 + 1;
  l0 = clamp(l0, 0, img->levels - 1);
  l1 = clamp(l1, 0, img->levels - 1);
  return linear( pixel2l(img, l0, y / (1 << l0), x / (1 << l0), c)
               , pixel2l(img, l1, y / (1 << l1), x / (1 << l1), c), lt);
}

int main(int argc, char **argv) {
  if (argc != 5) exit(1);
  char *stem = argv[1];
  int ow = atoi(argv[2]);
  int oh = atoi(argv[3]);
  float speed = atof(argv[4]);
  struct image *img = loadppms(stem);
  unsigned char *out = malloc(ow * oh * 3);
  float SR = 48000;
  float fps = 60;
  float *au = malloc((int)roundf(SR/fps) * 2 * sizeof(float));
  char header[1024];
  snprintf(header, 1000, "P6\n%d %d 255\n", ow, oh);
  float h = 1.0 / sqrt(ow * ow + oh * oh);
  float k = img->width / 8.0;
  float dy = speed * k / fps;

  int owoh = ow * oh;
  float *ds = malloc(owoh * sizeof(*ds));
  float *xs = malloc(owoh * sizeof(*xs));
  float *ls = malloc(owoh * sizeof(*ls));
  for (int j = 0; j < oh; ++j) {
    for (int i = 0; i < ow; ++i) {
      int k = j * ow + i;
      float p = (i - (ow >> 1) + 0.5f) * h;
      float q = ((oh >> 1) - j - 0.5f) * h;
      ds[k] = (logf(sqrtf(p * p + q * q)) * 0.15915494309189535f) * img->width;
      xs[k] = (atan2f(-q, -p) * 0.15915494309189535f + 0.5f) * img->width;
      ls[k] = - log2f(sqrtf(p * p + q * q)) - 2;
    }
  }

  for (float y0 = 0; y0 < img->height - img->width; y0 += dy)
  {
    #pragma omp parallel for
    for (int k = 0; k < owoh; ++k)
    {
      for (int c = 0; c < 3; ++c)
      {
        out[k * 3 + c] = clamp(pixel2(img, ls[k], y0 - ds[k], xs[k], c) * 255, 0, 255);
      }
    }
    if (1 != fwrite(header, strlen(header), 1, stdout)) exit(2);
    if (1 != fwrite(out, ow * oh * 3, 1, stdout)) exit(3);
    {
      float l = -log2(img->width / roundf(SR / fps / 2));
      for (int s = 0; s < SR/fps; ++s) {
        for (int c = 0; c < 2; ++c) {
          float x = fmod(s / roundf(SR / fps / 2) + 0.5 * c, 1) * img->width;
          float y = y0 + dy * (s / roundf(SR / fps));
          float a = pixel2(img, l, y, x, 0) + pixel2(img, l, y, x, 1) + pixel2(img, l, y, x, 2);
          if (a == 3) a = 0;
          au[2 * s + c] = a / 3.0 - 0.5;
        }
      }
    }
    if (1 != fwrite(au, (int)roundf(SR/fps) * 2 * sizeof(float), 1, stderr)) exit(6);
  }
  return 0;
}
