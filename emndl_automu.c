#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mpfr.h>

#if MPFR_VERSION_MAJOR < 3
#define MPFR_RNDN GMP_RNDN
#endif

int max(int a, int b) { return a > b ? a : b; }

void progressReport(int c) { fputc(c, stderr); fflush(stderr); }

int muatom(int period, mpfr_t x, mpfr_t y, mpfr_t z) {
  const mpfr_prec_t accuracy = 12;
  mpfr_prec_t bits = max(mpfr_get_prec(x), mpfr_get_prec(y));
#define VARS nx, ny, bx, by, wx, wy, zz, Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, Ex, Ey, t1, t2, t3, t4, t5, t6, t7, t8, t9, t0, t01, t02, t03, t04
  mpfr_t VARS;
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_set(nx, x, MPFR_RNDN);
  mpfr_set(ny, y, MPFR_RNDN);
  mpfr_set_prec(zz, mpfr_get_prec(z));
  int n_ok, w_ok, b_ok;
  int r = 0;
  progressReport('\n');
  do { progressReport('X');
    mpfr_set_prec(t0,  bits - accuracy);
    mpfr_set_prec(t01, bits - accuracy);
    mpfr_set_prec(t02, bits - accuracy);
    mpfr_set_prec(t03, bits - accuracy);
    mpfr_set_prec(t04, bits - accuracy);
    int limit = 40;
    do { progressReport('n');
      // refine nucleus
      mpfr_set_ui(Ax, 0, MPFR_RNDN);
      mpfr_set_ui(Ay, 0, MPFR_RNDN);
      mpfr_set_ui(Dx, 0, MPFR_RNDN);
      mpfr_set_ui(Dy, 0, MPFR_RNDN);
      for (int i = 0; i < period; ++i) {
        // D <- 2AD + 1  : Dx <- 2 (Ax Dx - Ay Dy) + 1 ; Dy = 2 (Ax Dy + Ay Dx)
        mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
        mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
        mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
        mpfr_sub(Dx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add(Dy, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
        // A <- A^2 + n  : Ax <- Ax^2 - Ay^2 + nx ; Ay <- 2 Ax Ay + ny
        mpfr_sqr(t1, Ax, MPFR_RNDN);
        mpfr_sqr(t2, Ay, MPFR_RNDN);
        mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
        mpfr_sub(Ax, t1, t2, MPFR_RNDN);
        mpfr_add(Ax, Ax, nx, MPFR_RNDN);
        mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
        mpfr_add(Ay, t3, ny,MPFR_RNDN);
      }
      // n <- n - A / D  = (nx + ny i) - ((Ax Dx + Ay Dy) + (Ay Dx - Ax Dy)i) / (Dx^2 + Dy^2)
      mpfr_sqr(t1, Dx, MPFR_RNDN);
      mpfr_sqr(t2, Dy, MPFR_RNDN);
      mpfr_add(t1, t1, t2, MPFR_RNDN);
  
      mpfr_mul(t2, Ax, Dx, MPFR_RNDN);
      mpfr_mul(t3, Ay, Dy, MPFR_RNDN);
      mpfr_add(t2, t2, t3, MPFR_RNDN);
      mpfr_div(t2, t2, t1, MPFR_RNDN);
      mpfr_sub(t2, nx, t2, MPFR_RNDN);
  
      mpfr_mul(t3, Ay, Dx, MPFR_RNDN);
      mpfr_mul(t4, Ax, Dy, MPFR_RNDN);
      mpfr_sub(t3, t3, t4, MPFR_RNDN);
      mpfr_div(t3, t3, t1, MPFR_RNDN);
      mpfr_sub(t3, ny, t3, MPFR_RNDN);
  
      mpfr_set(t01, t2, MPFR_RNDN);
      mpfr_set(t02, t3, MPFR_RNDN);
      mpfr_set(t03, nx, MPFR_RNDN);
      mpfr_set(t04, ny, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      n_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(nx, t2, MPFR_RNDN);
      mpfr_set(ny, t3, MPFR_RNDN);
    } while (! n_ok && --limit);
    if (! limit) goto cleanup;
    mpfr_set(wx, nx, MPFR_RNDN);
    mpfr_set(wy, ny, MPFR_RNDN);
    mpfr_set(bx, nx, MPFR_RNDN);
    mpfr_set(by, ny, MPFR_RNDN);
    limit = 40;
    do { progressReport('b');
      // refine bond
      mpfr_set(Ax, wx, MPFR_RNDN);
      mpfr_set(Ay, wy, MPFR_RNDN);
      mpfr_set_ui(Bx, 1, MPFR_RNDN);
      mpfr_set_ui(By, 0, MPFR_RNDN);
      mpfr_set_ui(Cx, 0, MPFR_RNDN);
      mpfr_set_ui(Cy, 0, MPFR_RNDN);
      mpfr_set_ui(Dx, 0, MPFR_RNDN);
      mpfr_set_ui(Dy, 0, MPFR_RNDN);
      mpfr_set_ui(Ex, 0, MPFR_RNDN);
      mpfr_set_ui(Ey, 0, MPFR_RNDN);
      for (int i = 0; i < period; ++i) {
        // E <- 2 ( A E + B D )
        mpfr_mul(t1, Ax, Ex, MPFR_RNDN);
        mpfr_mul(t2, Ay, Ey, MPFR_RNDN);
        mpfr_sub(t1, t1, t2, MPFR_RNDN);
        mpfr_mul(t2, Ax, Ey, MPFR_RNDN);
        mpfr_mul(t3, Ay, Ex, MPFR_RNDN);
        mpfr_add(t2, t2, t3, MPFR_RNDN);
        mpfr_mul(t3, Bx, Dx, MPFR_RNDN);
        mpfr_mul(t4, By, Dy, MPFR_RNDN);
        mpfr_sub(t3, t3, t4, MPFR_RNDN);
        mpfr_add(Ex, t1, t3, MPFR_RNDN);
        mpfr_mul(t3, Bx, Dy, MPFR_RNDN);
        mpfr_mul(t4, By, Dx, MPFR_RNDN);
        mpfr_add(t3, t3, t4, MPFR_RNDN);
        mpfr_add(Ey, t2, t3, MPFR_RNDN);
        mpfr_mul_2ui(Ex, Ex, 1, MPFR_RNDN);
        mpfr_mul_2ui(Ey, Ey, 1, MPFR_RNDN);
        // D <- 2 A D + 1
        mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
        mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
        mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
        mpfr_sub(Dx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add(Dy, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
        // C <- 2 ( B^2 + A C )
        mpfr_sqr(t1, Bx, MPFR_RNDN);
        mpfr_sqr(t2, By, MPFR_RNDN);
        mpfr_sub(t1, t1, t2, MPFR_RNDN);
        mpfr_mul(t2, Bx, By, MPFR_RNDN);
        mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
        mpfr_mul(t3, Ax, Cx, MPFR_RNDN);
        mpfr_mul(t4, Ay, Cy, MPFR_RNDN);
        mpfr_sub(t3, t3, t4, MPFR_RNDN);
        mpfr_add(t1, t1, t3, MPFR_RNDN);
        mpfr_mul(t3, Ax, Cy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Cx, MPFR_RNDN);
        mpfr_add(t3, t3, t4, MPFR_RNDN);
        mpfr_add(t2, t2, t3, MPFR_RNDN);
        mpfr_mul_2ui(Cx, t1, 1, MPFR_RNDN);
        mpfr_mul_2ui(Cy, t2, 1, MPFR_RNDN);
        // B <- 2 A B
        mpfr_mul(t1, Ax, Bx, MPFR_RNDN);
        mpfr_mul(t2, Ay, By, MPFR_RNDN);
        mpfr_mul(t3, Ax, By, MPFR_RNDN);
        mpfr_mul(t4, Ay, Bx, MPFR_RNDN);
        mpfr_sub(Bx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Bx, Bx, 1, MPFR_RNDN);
        mpfr_add(By, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(By, By, 1, MPFR_RNDN);
        // A <- A^2 + b
        mpfr_sqr(t1, Ax, MPFR_RNDN);
        mpfr_sqr(t2, Ay, MPFR_RNDN);
        mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
        mpfr_sub(Ax, t1, t2, MPFR_RNDN);
        mpfr_add(Ax, Ax, bx, MPFR_RNDN);
        mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
        mpfr_add(Ay, t3, by,MPFR_RNDN);
      }
      // (w) <- (w) - (B-1  D)^-1 (A - w)
      // (b) <- (b)   ( C   E)    (B + 1)
      //
      // det = (B-1)E - CD
      // inv = (E    -D)
      //       (-C (B-1) / det
      //
      // w - (E(A-w) - D(B+1))/det
      // b - (-C(A-w) + (B-1)(B+1))/det   ; B^2 - 1
      //
      // A -= w
      mpfr_sub(Ax, Ax, wx, MPFR_RNDN);
      mpfr_sub(Ay, Ay, wy, MPFR_RNDN);
      // (t1,t2) = B^2 - 1
      mpfr_mul(t1, Bx, Bx, MPFR_RNDN);
      mpfr_mul(t2, By, By, MPFR_RNDN);
      mpfr_sub(t1, t1, t2, MPFR_RNDN);
      mpfr_sub_ui(t1, t1, 1, MPFR_RNDN);
      mpfr_mul(t2, Bx, By, MPFR_RNDN);
      mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
      // (t1,t2) -= C(A-w)
      mpfr_mul(t3, Cx, Ax, MPFR_RNDN);
      mpfr_mul(t4, Cy, Ay, MPFR_RNDN);
      mpfr_sub(t3, t3, t4, MPFR_RNDN);
      mpfr_sub(t1, t1, t3, MPFR_RNDN);
      mpfr_mul(t3, Cx, Ay, MPFR_RNDN);
      mpfr_mul(t4, Cy, Ax, MPFR_RNDN);
      mpfr_add(t3, t3, t4, MPFR_RNDN);
      mpfr_sub(t2, t2, t3, MPFR_RNDN);
      // (t3, t4) = (B-1)E - CD
      mpfr_sub_ui(t3, Bx, 1, MPFR_RNDN);
      mpfr_mul(t4, t3, Ex, MPFR_RNDN);
      mpfr_mul(t5, By, Ey, MPFR_RNDN);
      mpfr_sub(t4, t4, t5, MPFR_RNDN);
      mpfr_mul(t5, t3, Ey, MPFR_RNDN);
      mpfr_mul(t6, By, Ex, MPFR_RNDN);
      mpfr_sub(t5, t5, t6, MPFR_RNDN);
      mpfr_mul(t6, Cx, Dx, MPFR_RNDN);
      mpfr_mul(t7, Cy, Dy, MPFR_RNDN);
      mpfr_sub(t6, t6, t7, MPFR_RNDN);
      mpfr_sub(t3, t4, t6, MPFR_RNDN);
      mpfr_mul(t6, Cx, Dy, MPFR_RNDN);
      mpfr_mul(t7, Cy, Dx, MPFR_RNDN);
      mpfr_add(t6, t6, t7, MPFR_RNDN);
      mpfr_sub(t4, t5, t6, MPFR_RNDN);
      // (t3, t4) = 1/(t3, t4)  ; z^-1 = z* / (z z*)
      mpfr_sqr(t5, t3, MPFR_RNDN);
      mpfr_sqr(t6, t4, MPFR_RNDN);
      mpfr_add(t5, t5, t6, MPFR_RNDN);
      mpfr_div(t3, t3, t5, MPFR_RNDN);
      mpfr_div(t4, t4, t5, MPFR_RNDN);
      mpfr_neg(t4, t4, MPFR_RNDN);
      // (t1, t2) *= (t3, t4)
      mpfr_mul(t5, t1, t3, MPFR_RNDN);
      mpfr_mul(t6, t2, t4, MPFR_RNDN);
      mpfr_mul(t7, t1, t4, MPFR_RNDN);
      mpfr_mul(t8, t2, t3, MPFR_RNDN);
      mpfr_sub(t1, t5, t6, MPFR_RNDN);
      mpfr_add(t2, t7, t8, MPFR_RNDN);
  
      // (t1, t2) = b - (t1, t2)
      mpfr_sub(t1, bx, t1, MPFR_RNDN);
      mpfr_sub(t2, by, t2, MPFR_RNDN);
  
      mpfr_set(t01, t1, MPFR_RNDN);
      mpfr_set(t02, t2, MPFR_RNDN);
      mpfr_set(t03, bx, MPFR_RNDN);
      mpfr_set(t04, by, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      b_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      // (t5, t6) = E (A-w)
      mpfr_mul(t5, Ex, Ax, MPFR_RNDN);
      mpfr_mul(t6, Ey, Ay, MPFR_RNDN);
      mpfr_sub(t5, t5, t6, MPFR_RNDN);
      mpfr_mul(t6, Ex, Ay, MPFR_RNDN);
      mpfr_mul(t7, Ey, Ax, MPFR_RNDN);
      mpfr_add(t6, t6, t7, MPFR_RNDN);
      // B += 1
      mpfr_add_ui(Bx, Bx, 1, MPFR_RNDN);
      // (t7, t8) = D (B+1)
      mpfr_mul(t7, Dx, Bx, MPFR_RNDN);
      mpfr_mul(t8, Dy, By, MPFR_RNDN);
      mpfr_sub(t7, t7, t8, MPFR_RNDN);
      mpfr_mul(t8, Dx, By, MPFR_RNDN);
      mpfr_mul(t9, Dy, Bx, MPFR_RNDN);
      mpfr_add(t8, t8, t9, MPFR_RNDN);
      // (t5, t6) -= (t7, t8)
      mpfr_sub(t5, t5, t7, MPFR_RNDN);
      mpfr_sub(t6, t6, t8, MPFR_RNDN);
      // (t7, t8) = (t5, t6) * (t3, t4)
      mpfr_mul(t7, t3, t5, MPFR_RNDN);
      mpfr_mul(t8, t4, t6, MPFR_RNDN);
      mpfr_sub(t7, t7, t8, MPFR_RNDN);
      mpfr_mul(t8, t3, t6, MPFR_RNDN);
      mpfr_mul(t9, t4, t5, MPFR_RNDN);
      mpfr_add(t8, t8, t9, MPFR_RNDN);
  
      // (t3, t4) = w - (t7, t8)
      mpfr_sub(t3, wx, t7, MPFR_RNDN);
      mpfr_sub(t4, wy, t8, MPFR_RNDN);
  
      mpfr_set(t01, t3, MPFR_RNDN);
      mpfr_set(t02, t4, MPFR_RNDN);
      mpfr_set(t03, wx, MPFR_RNDN);
      mpfr_set(t04, wy, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      w_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(t01, t3, MPFR_RNDN);
      mpfr_set(t02, t4, MPFR_RNDN);
      mpfr_set(t03, wx, MPFR_RNDN);
      mpfr_set(t04, wy, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      w_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(bx, t1, MPFR_RNDN);
      mpfr_set(by, t2, MPFR_RNDN);
      mpfr_set(wx, t3, MPFR_RNDN);
      mpfr_set(wy, t4, MPFR_RNDN);
    } while (!(w_ok && b_ok) && --limit);
    if (! limit) goto cleanup;
    // t1 = |n - b|
    mpfr_sub(t1, nx, bx, MPFR_RNDN);
    mpfr_sqr(t1, t1, MPFR_RNDN);
    mpfr_sub(t2, ny, by, MPFR_RNDN);
    mpfr_sqr(t2, t2, MPFR_RNDN);
    mpfr_add(t1, t1, t2, MPFR_RNDN);
    mpfr_sqrt(t1, t1, MPFR_RNDN);
    bits = bits + accuracy;
    mpfr_prec_round(nx, bits, MPFR_RNDN);
    mpfr_prec_round(ny, bits, MPFR_RNDN);
    mpfr_prec_round(wx, bits, MPFR_RNDN);
    mpfr_prec_round(wy, bits, MPFR_RNDN);
    mpfr_prec_round(bx, bits, MPFR_RNDN);
    mpfr_prec_round(by, bits, MPFR_RNDN);
    mpfr_set(zz, t1, MPFR_RNDN);
    mpfr_set_prec(Ax, bits);
    mpfr_set_prec(Ay, bits);
    mpfr_set_prec(Bx, bits);
    mpfr_set_prec(By, bits);
    mpfr_set_prec(Cx, bits);
    mpfr_set_prec(Cy, bits);
    mpfr_set_prec(Dx, bits);
    mpfr_set_prec(Dy, bits);
    mpfr_set_prec(Ex, bits);
    mpfr_set_prec(Ey, bits);
    mpfr_set_prec(t1, bits);
    mpfr_set_prec(t2, bits);
    mpfr_set_prec(t3, bits);
    mpfr_set_prec(t4, bits);
    mpfr_set_prec(t5, bits);
    mpfr_set_prec(t6, bits);
    mpfr_set_prec(t7, bits);
    mpfr_set_prec(t8, bits);
    mpfr_set_prec(t9, bits);
  } while (mpfr_zero_p(zz) || bits + mpfr_get_exp(zz) < mpfr_get_prec(zz) + accuracy);

  // copy to output
  bits = accuracy - mpfr_get_exp(zz);
  r = 1;
  mpfr_set_prec(x, bits);
  mpfr_set_prec(y, bits);
  mpfr_set(x, nx, MPFR_RNDN);
  mpfr_set(y, ny, MPFR_RNDN);
  mpfr_set(z, zz, MPFR_RNDN);
cleanup:
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return r;
}

#define LOGSIZE 8
#define SIZE (1 << LOGSIZE)

struct pixel {
  enum { done, active, queued } state;
  enum { white, black } colour;
  int n;
  mpfr_t cx, cy, zx, zy, dx, dy;
};

struct image {
  struct pixel p[SIZE][SIZE*2];
};

void initialize(struct image *img, int bits) {
  for (int j = 0; j < SIZE; ++j) {
    for (int i = 0; i < SIZE*2; ++i) {
      mpfr_init2(img->p[j][i].cx, bits);
      mpfr_init2(img->p[j][i].cy, bits);
      mpfr_init2(img->p[j][i].zx, bits);
      mpfr_init2(img->p[j][i].zy, bits);
      mpfr_init2(img->p[j][i].dx, bits);
      mpfr_init2(img->p[j][i].dy, bits);
    }
  }
}

void deinitialize(struct image *img) {
  for (int j = 0; j < SIZE; ++j) {
    for (int i = 0; i < SIZE*2; ++i) {
      mpfr_clear(img->p[j][i].cx);
      mpfr_clear(img->p[j][i].cy);
      mpfr_clear(img->p[j][i].zx);
      mpfr_clear(img->p[j][i].zy);
      mpfr_clear(img->p[j][i].dx);
      mpfr_clear(img->p[j][i].dy);
    }
  }
}

void render(struct image *img, mpfr_t x, mpfr_t y, mpfr_t z) {
  mpfr_prec_t bits = max(mpfr_get_prec(x), mpfr_get_prec(y)) - 8;
  mpfr_t cx0, cy0, cz0, zx2, zy2, zxy, zmag2, dzx, dzy, t;
  mpfr_inits2(bits, cx0, cy0, cz0, zx2, zy2, zxy, zmag2, dzx, dzy, t, (mpfr_ptr) 0);
  mpfr_set(cx0, x, MPFR_RNDN);
  mpfr_set(cy0, y, MPFR_RNDN);
  mpfr_set(cz0, z, MPFR_RNDN);
  mpfr_div_2ui(cz0, cz0, LOGSIZE, MPFR_RNDN);
  mpfr_mul_ui(cz0, cz0, 6, MPFR_RNDN);
  double rx = rand() / (double) RAND_MAX;
  double ry = rand() / (double) RAND_MAX;
  for (int j = 0; j < SIZE; ++j) {
    for (int i = 0; i < SIZE*2; ++i) {
      img->p[j][i].state = (i == 0 || j == 0 || i == SIZE*2 - 1 || j == SIZE - 1) ? active : queued;
      img->p[j][i].colour = white;
      img->p[j][i].n = 0;
      mpfr_set_prec(img->p[j][i].cx, bits);
      mpfr_set_prec(img->p[j][i].cy, bits);
      mpfr_set_prec(img->p[j][i].zx, bits);
      mpfr_set_prec(img->p[j][i].zy, bits);
      mpfr_set_prec(img->p[j][i].dx, bits);
      mpfr_set_prec(img->p[j][i].dy, bits);
      mpfr_set(img->p[j][i].cx, cz0, MPFR_RNDN);
      mpfr_mul_d(img->p[j][i].cx, img->p[j][i].cx, (i - SIZE + rx)/2, MPFR_RNDN);
      mpfr_add(img->p[j][i].cx, img->p[j][i].cx, cx0, MPFR_RNDN);
      mpfr_set(img->p[j][i].cy, cz0, MPFR_RNDN);
      mpfr_mul_d(img->p[j][i].cy, img->p[j][i].cy, j - SIZE/2 + ry, MPFR_RNDN);
      mpfr_add(img->p[j][i].cy, img->p[j][i].cy, cy0, MPFR_RNDN);
      mpfr_set_ui(img->p[j][i].zx, 0, MPFR_RNDN);
      mpfr_set_ui(img->p[j][i].zy, 0, MPFR_RNDN);
      mpfr_set_ui(img->p[j][i].dx, 0, MPFR_RNDN);
      mpfr_set_ui(img->p[j][i].dy, 0, MPFR_RNDN);
    }
  }
  int progressed = 0;
  int progress = 1;
  int maxiters = 256;
  while ((!progressed) || progress > LOGSIZE) {
    progressReport('.');
    progress = 0;
    int finished = 0;
    while (! finished) {
      finished = 1;
      for (int j = 0; j < SIZE; ++j) {
        for (int i = 0; i < SIZE*2; ++i) {
          if (active == img->p[j][i].state && img->p[j][i].n < maxiters) {
            finished = 0;
            int n = img->p[j][i].n;
            mpfr_t *cx = &img->p[j][i].cx;
            mpfr_t *cy = &img->p[j][i].cy;
            mpfr_t *zx = &img->p[j][i].zx;
            mpfr_t *zy = &img->p[j][i].zy;
            mpfr_t *dx = &img->p[j][i].dx;
            mpfr_t *dy = &img->p[j][i].dy;
            // |z|^2
            mpfr_sqr(zx2, *zx, MPFR_RNDN);
            mpfr_sqr(zy2, *zy, MPFR_RNDN);
            mpfr_add(zmag2, zx2, zy2, MPFR_RNDN);
            while (n < maxiters && mpfr_cmp_d(zmag2, 65536) < 0 ) {
              // d <- 2 d z + 1
              mpfr_mul(dzx, *zx, *dx, MPFR_RNDN);
              mpfr_mul(t, *zy, *dy, MPFR_RNDN);
              mpfr_sub(dzx, dzx, t, MPFR_RNDN);
              mpfr_mul(dzy, *zx, *dy, MPFR_RNDN);
              mpfr_mul(t, *zy, *dx, MPFR_RNDN);
              mpfr_add(dzy, dzy, t, MPFR_RNDN);
              mpfr_mul_2ui(dzx, dzx, 1, MPFR_RNDN);
              mpfr_add_ui(*dx, dzx, 1, MPFR_RNDN);
              mpfr_mul_2ui(*dy, dzy, 1, MPFR_RNDN);
              // z <- z^2 + c
              mpfr_mul(zxy, *zx, *zy, MPFR_RNDN);
              mpfr_sub(*zx, zx2, zy2, MPFR_RNDN);
              mpfr_add(*zx, *zx, *cx, MPFR_RNDN);
              mpfr_mul_2ui(*zy, zxy, 1, MPFR_RNDN);
              mpfr_add(*zy, *zy, *cy, MPFR_RNDN);
              // |z|^2
              mpfr_sqr(zx2, *zx, MPFR_RNDN);
              mpfr_sqr(zy2, *zy, MPFR_RNDN);
              mpfr_add(zmag2, zx2, zy2, MPFR_RNDN);
              // count
              ++n;
            }
            img->p[j][i].n = n;
            if (!(mpfr_cmp_d(zmag2, 65536) < 0)) {
              // escaped
              mpfr_sqr(*dx, *dx, MPFR_RNDN);
              mpfr_sqr(*dy, *dy, MPFR_RNDN);
              mpfr_add(*dx, *dx, *dy, MPFR_RNDN);
              mpfr_div(*dx, zmag2, *dx, MPFR_RNDN);
              mpfr_sqrt(*dx, *dx, MPFR_RNDN);
              mpfr_log(zmag2, zmag2, MPFR_RNDN);
              mpfr_mul(zmag2, zmag2, *dx, MPFR_RNDN);
              mpfr_div(zmag2, zmag2, cz0, MPFR_RNDN);
              double distance = mpfr_get_d(zmag2, MPFR_RNDN);
              img->p[j][i].colour = (distance < 0.5 * sqrt(2)) ? black : white;
              img->p[j][i].state = done;
              for (int dj = -1; dj <= 1; ++dj) {
                int jj = j + dj;
                if (jj < 0 || SIZE <= jj) continue;
                for (int di = -1; di <= 1; ++di) {
                  int ii = i + di;
                  if (ii < 0 || SIZE*2 <= ii) continue;
                  if (img->p[jj][ii].state == queued) {
                    img->p[jj][ii].state = active;
                  }
                }
              }
              ++progress;
            }
          }
        }
      }
    }
    maxiters *= 2;
    progressed = progressed || progress;
  }
  progressReport('\n');
  mpfr_clears(cx0, cy0, cz0, zx2, zy2, zxy, zmag2, dzx, dzy, t, (mpfr_ptr) 0);
  const unsigned char blocks[16][4] =
    { { 0x20, 0x00, 0x00, 0 } // empty
    , { 0xe2, 0x96, 0x98, 0 } // top left
    , { 0xe2, 0x96, 0x9d, 0 } // top right
    , { 0xe2, 0x96, 0x80, 0 } // top
    , { 0xe2, 0x96, 0x96, 0 } // bottom left
    , { 0xe2, 0x96, 0x8c, 0 } // left
    , { 0xe2, 0x96, 0x9e, 0 } // bottom left top right
    , { 0xe2, 0x96, 0x9b, 0 } // not bottom right
    , { 0xe2, 0x96, 0x97, 0 } // bottom right
    , { 0xe2, 0x96, 0x9a, 0 } // top left bottom right
    , { 0xe2, 0x96, 0x90, 0 } // right
    , { 0xe2, 0x96, 0x9c, 0 } // not bottom left
    , { 0xe2, 0x96, 0x84, 0 } // bottom
    , { 0xe2, 0x96, 0x99, 0 } // not top right
    , { 0xe2, 0x96, 0x9f, 0 } // not top left
    , { 0xe2, 0x96, 0x88, 0 } // full
    };
  for (int j = SIZE - 1; j > 0; j -= 2) {
    for (int i = 0; i < SIZE*2 - 1 && i < 360; i += 2) {
      int b =  (img->p[j][i].colour == black)
            | ((img->p[j][i+1].colour == black) << 1)
            | ((img->p[j-1][i].colour == black) << 2)
            | ((img->p[j-1][i+1].colour == black) << 3);
      fprintf(stderr, "%s", blocks[b]);
    }
    fputc('\n', stderr);
  }
  fflush(stderr);
}

int locate(mpfr_t x, mpfr_t y, mpfr_t z, int *period) {
  progressReport('L');
  mpfr_prec_t bits = max(mpfr_get_prec(x), mpfr_get_prec(y)) + 8;
  mpfr_prec_round(x, bits, MPFR_RNDN);
  mpfr_prec_round(y, bits, MPFR_RNDN);
  mpfr_t cz0, zx[4], zy[4], cx[4], cy[4], dx, dy, t;
  mpfr_inits2(bits, cz0, zx[0], zx[1], zx[2], zx[3], zy[0], zy[1], zy[2], zy[3], cx[0], cx[1], cx[2], cx[3], cy[0], cy[1], cy[2], cy[3], dx, dy, t, (mpfr_ptr) 0);
  mpfr_set(cz0, z, MPFR_RNDN);
  mpfr_sub(cx[0], x, cz0, MPFR_RNDN);
  mpfr_sub(cy[0], y, cz0, MPFR_RNDN);
  mpfr_add(cx[1], x, cz0, MPFR_RNDN);
  mpfr_sub(cy[1], y, cz0, MPFR_RNDN);
  mpfr_add(cx[2], x, cz0, MPFR_RNDN);
  mpfr_add(cy[2], y, cz0, MPFR_RNDN);
  mpfr_sub(cx[3], x, cz0, MPFR_RNDN);
  mpfr_add(cy[3], y, cz0, MPFR_RNDN);
  mpfr_set_ui(zx[0], 0, MPFR_RNDN);
  mpfr_set_ui(zy[0], 0, MPFR_RNDN);
  mpfr_set_ui(zx[1], 0, MPFR_RNDN);
  mpfr_set_ui(zy[1], 0, MPFR_RNDN);
  mpfr_set_ui(zx[2], 0, MPFR_RNDN);
  mpfr_set_ui(zy[2], 0, MPFR_RNDN);
  mpfr_set_ui(zx[3], 0, MPFR_RNDN);
  mpfr_set_ui(zy[3], 0, MPFR_RNDN);
  int origin;
  int p0 = *period;
  int p = 0;
  do {
    ++p;
    for (int i = 0; i < 4; ++i) {
      mpfr_mul(t, zx[i], zy[i], MPFR_RNDN);
      mpfr_sqr(zx[i], zx[i], MPFR_RNDN);
      mpfr_sqr(zy[i], zy[i], MPFR_RNDN);
      mpfr_sub(zx[i], zx[i], zy[i], MPFR_RNDN);
      mpfr_add(zx[i], zx[i], cx[i], MPFR_RNDN);
      mpfr_mul_2ui(zy[i], t, 1, MPFR_RNDN);
      mpfr_add(zy[i], zy[i], cy[i], MPFR_RNDN);
    }
    int preal = 0;
    for (int i = 0; i < 4; ++i) {
      int j = (i + 1) % 4;
      if (!(mpfr_sgn(zy[i]) < 0 && mpfr_sgn(zy[j]) < 0) && !(mpfr_sgn(zy[i]) > 0 && mpfr_sgn(zy[j]) > 0)) {
        mpfr_sub(dy, zy[j], zy[i], MPFR_RNDN);
        mpfr_sub(dx, zx[j], zx[i], MPFR_RNDN);
        int s = mpfr_sgn(dy);
        mpfr_mul(dy, dy, zx[i], MPFR_RNDN);
        mpfr_mul(dx, dx, zy[i], MPFR_RNDN);
        mpfr_sub(dy, dy, dx, MPFR_RNDN);
        if (mpfr_sgn(dy) == s && s != 0) {
          ++preal;
        }
      }
    }
    origin = preal & 1;
  } while ((!origin) && p < p0 * 12);
  if (origin) {
    *period = p;
  }
  mpfr_clears(cz0, zx[0], zx[1], zx[2], zx[3], zy[0], zy[1], zy[2], zy[3], cx[0], cx[1], cx[2], cx[3], cy[0], cy[1], cy[2], cy[3], (mpfr_ptr) 0);
  return origin;
}

int pick(struct image *img, mpfr_t x, mpfr_t y, mpfr_t z, int *period) {
  mpfr_prec_t bits = max(mpfr_get_prec(x), mpfr_get_prec(y)) + 8;
  mpfr_prec_round(x, bits, MPFR_RNDN);
  mpfr_prec_round(y, bits, MPFR_RNDN);
  mpfr_t cz0;
  mpfr_init2(cz0, mpfr_get_prec(z));
  mpfr_set(cz0, z, MPFR_RNDN);
  mpfr_div_2ui(cz0, cz0, LOGSIZE, MPFR_RNDN);
  mpfr_mul_ui(cz0, cz0, 6, MPFR_RNDN);
  int r = 0;
  int n = 0;
  for (int j = 0; j < SIZE; ++j) {
    for (int i = 0; i < SIZE*2; ++i) {
      if (img->p[j][i].colour == black) {
        ++n;
      }
    }
  }
  if (n > 0) {
    int p0 = *period;
    int p = 0;
    while (n > 0 && (p < p0 || (p0 > 1 && (p % p0) == 0) || p > p0 * 8)) {
      progressReport(',');
      int m = rand() % n + 1;
      for (int j = 0; j < SIZE; ++j) {
        for (int i = 0; i < SIZE*2; ++i) {
          if (img->p[j][i].colour == black) {
            --m;
          }
          if (m == 0) {
            --n;
            img->p[j][i].colour = white;
            mpfr_set(x, img->p[j][i].cx, MPFR_RNDN);
            mpfr_set(y, img->p[j][i].cy, MPFR_RNDN);
            break;
          }
        }
        if (m == 0) {
          break;
        }
      }
      if (m == 0) {
        p = p0;
        if (! locate(x, y, cz0, &p)) {
          p = 0;
          r = 0;
        } else {
          r = 1;
        }
      } else {
        puts("internal error in pick()\n");
        exit(1);
      }
    }
    *period = p;
  }
  mpfr_clear(cz0);
  return r;
}

void main2(mpfr_prec_t prec0, int period, mpfr_t x, mpfr_t y, mpfr_t z, struct image *img) {
  if (muatom(period, x, y, z)) {
    int period2 = period;
    mpfr_t x2, y2, z2;
    mpfr_init2(x2, mpfr_get_prec(x)); mpfr_set(x2, x, MPFR_RNDN);
    mpfr_init2(y2, mpfr_get_prec(y)); mpfr_set(y2, y, MPFR_RNDN);
    mpfr_init2(z2, mpfr_get_prec(z)); mpfr_set(z2, z, MPFR_RNDN);
    int bits = mpfr_get_prec(x);
    mpfr_printf("%d %d %Re %Re %Re\n", bits, period, x, y, z);
    fflush(stdout);
    if (bits > prec0) exit(0);
    if (period == 1) {
      initialize(img, 24);
      render(img, x, y, z);
      while (pick(img, x, y, z, &period)) {
        main2(prec0, period, x, y, z, img);
        period = period2;
        mpfr_set_prec(x, mpfr_get_prec(x2)); mpfr_set(x, x2, MPFR_RNDN);
        mpfr_set_prec(y, mpfr_get_prec(y2)); mpfr_set(y, y2, MPFR_RNDN);
        mpfr_set_prec(z, mpfr_get_prec(z2)); mpfr_set(z, z2, MPFR_RNDN);
      }
      deinitialize(img);
    } else {
      mpfr_prec_round(x, mpfr_get_prec(x) * 2, MPFR_RNDN);
      mpfr_prec_round(y, mpfr_get_prec(y) * 2, MPFR_RNDN);
      for (int j = 0; j < 8 * SIZE; ++j) {
        double angle  = 6.283185307179586 * rand() / (double) RAND_MAX;
        double radius = (1.0 + rand() / (double) RAND_MAX) / 0.75;
        mpfr_t p;
        mpfr_init2(p, mpfr_get_prec(z2));
        mpfr_set_d(p, 0.995, MPFR_RNDN);
        mpfr_pow(x, z2, p, MPFR_RNDN);
        mpfr_pow(y, z2, p, MPFR_RNDN);
        mpfr_clear(p);
        mpfr_set(z, z2, MPFR_RNDN);
        mpfr_mul_d(x, x, radius * cos(angle), MPFR_RNDN);
        mpfr_mul_d(y, y, radius * sin(angle), MPFR_RNDN);
        mpfr_add(x, x, x2, MPFR_RNDN);
        mpfr_add(y, y, y2, MPFR_RNDN);
        mpfr_div_d(z, z, log2(period), MPFR_RNDN);
        int period0 = period;
        if (locate(x, y, z, &period) && (period % period0)) {
          main2(prec0, period, x, y, z, img);
        }
        period = period2;
        mpfr_set_prec(x, mpfr_get_prec(x2)); mpfr_set(x, x2, MPFR_RNDN);
        mpfr_set_prec(y, mpfr_get_prec(y2)); mpfr_set(y, y2, MPFR_RNDN);
        mpfr_set_prec(z, mpfr_get_prec(z2)); mpfr_set(z, z2, MPFR_RNDN);
      }
    }
    mpfr_clear(x2);
    mpfr_clear(y2);
    mpfr_clear(z2);
  }
}

int main(int argc, char **argv) {
  srand(time(0));
  if (argc != 1 && argc != 5) {
    fprintf(stderr, "usage: ./emndl_automu prec period cx cy\n");
    return 1;
  }
  mpfr_prec_t prec = 16;
  if (argc == 5) {
    prec = atoi(argv[1]);
  }
  int period = 1;
  mpfr_t x, y, z;
  mpfr_inits2(prec, x, y, z, (mpfr_ptr) 0);
  mpfr_set_ui(x, 0, MPFR_RNDN);
  mpfr_set_ui(y, 0, MPFR_RNDN);
  if (argc == 5) {
    period = atoi(argv[2]);
    mpfr_set_str(x, argv[3], 10, MPFR_RNDN);
    mpfr_set_str(y, argv[4], 10, MPFR_RNDN);
  }
  mpfr_set_prec(z, 24);
  struct image *img = calloc(1, sizeof(*img));
  main2(prec, period, x, y, z, img);
  mpfr_clears(x, y, z, (mpfr_ptr) 0);
  mpfr_free_cache();
  return 0;
}
