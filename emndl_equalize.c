/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

struct pixel { float x; int i; };

int pixel_cmp_i(const void *a, const void *b) {
  const struct pixel *i = a;
  const struct pixel *j = b;
  if (i->i < j->i) return -1;
  if (i->i > j->i) return  1;
  return 0;
}

int pixel_cmp_x(const void *a, const void *b) {
  const struct pixel *i = a;
  const struct pixel *j = b;
  if (i->x < j->x) return -1;
  if (i->x > j->x) return  1;
  return 0;
}

int main(int argc, char **argv) {

  if (argc != 3) exit(1);

  FILE *fdi = fopen(argv[1], "rb");
  struct stat s;
  if (stat(argv[1], &s)) exit(1);
  int n = s.st_size / sizeof(float);
  struct pixel *p = calloc(n, sizeof(struct pixel));
  if (! p) exit(1);
  for (int i = 0; i < n; ++i) {
    float x;
    if (1 != fread(&x, sizeof(float), 1, fdi)) exit(1);
    p[i].x = x;
    p[i].i = i;
  }
  fclose(fdi);

  qsort(p, n, sizeof(struct pixel), pixel_cmp_x);
  int k = 0;
  while (p[k].x <= 0 && k < n) ++k;
  float f = 1.0 / (n - k);
  for (int i = k; i < n; ++i) {
    p[i].x = f * (1 + i - k);
  }
  qsort(p, n, sizeof(struct pixel), pixel_cmp_i);

  FILE *fdo = fopen(argv[2], "wb");
  for (int i = 0; i < n; ++i) {
    if (1 != fwrite(&p[i].x, sizeof(float), 1, fdo)) exit(1);
  }
  fclose(fdo);
  free(p);

  return 0;
}
