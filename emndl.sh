#!/bin/bash
#
#    emndl -- exponentially transformed Mandelbrot Set renderer
#    Copyright (C) 2011,2012,2018  Claude Heiland-Allen <claude@mathr.co.uk>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

TIME0="$(date --iso=s)"
E="$(dirname "$(readlink -f "$0")")"
OUT="emndl_${TIME0}"

version()
{
cat << EOF
emndl.sh git (GPL) 2011,2012,2018  Claude Heiland-Allen <claude@mathr.co.uk>
EOF
}

usage()
{
cat << EOF
usage options:
    --help -h -?    show this message and exit
    --version       show version and exit

input options:
    --auto bits     find an interesting point with precision 'bits'
    --re real       choose coordinates
    --im imag
    --period p
    --maxiters n
    --size sz

render options:
    --quality q     horizontal size, (5 - 12)

image options:
    --png           generate full size PNG
    --jpeg          generate thumbnail JPEG

video options:
    --aspect w:h    output aspect (4:3, 16:9)
    --speed s       zoom speed (default 1)
    --dvd           generate PAL DVD (SD)
    --mkv           generate Matroska (HD)
    --ogv           generate Theora (LD)

other options:
    --quiet         less output
    --verbose       more output
EOF
}

if [ "x$*" == "x" ]
then
  version
  cat << EOF
Try \`emndl.sh --help' for usage information.
EOF
  exit 0
fi

TEMP=`getopt -o '' --long help,version,auto:,re:,im:,period:,maxiters:,size:,quality:,png,jpeg,aspect:,speed:,dvd,mkv,ogv,quiet,verbose -n 'emndl.sh' -- "$@"`
if [ $? != 0 ]
then
  echo "error 1 in getopt" >&2
  exit 1
fi

eval set -- "$TEMP"

AUTO=0
AUTOBITS="128"
REAL=0
REALCOORD="0.0"
IMAG=0
IMAGCOORD="0.0"
PER=0
PERIOD=1
MAXITERS=1000
SZ=0
SIZE=1
QUALITY="8"
PNG=0
JPEG=0
ASPECT="16:9"
SPEED="1"
DVD=0
MKV=0
OGV=0
VERBOSE=1

while true
do
  case "$1" in
    --help|-h|-\?) usage ; exit 0 ;;
    --version) version ; exit 0 ;;
    --auto) AUTO=1 ; AUTOBITS="$2" ; shift 2 ;;
    --re) REAL=1 ; REALCOORD="$2" ; shift 2 ;;
    --im) IMAG=1 ; IMAGCOORD="$2" ; shift 2 ;;
    --period) PER=1; PERIOD="$2" ; shift 2 ;;
    --maxiters) MIT=1; MAXITERS="$2" ; shift 2 ;;
    --size) SZ=1; SIZE="$2" ; shift 2 ;;
    --quality) QUALITY="$2" ; shift 2 ;;
    --png) PNG=1 ; shift ;;
    --jpeg) JPEG=1 ; shift ;;
    --aspect) ASPECT="$2" ; shift 2 ;;
    --speed) SPEED="$2" ; shift 2 ;;
    --dvd) DVD=1 ; shift ;;
    --mkv) MKV=1 ; shift ;;
    --ogv) OGV=1 ; shift ;;
    --quiet) VERBOSE=0 ; shift ;;
    --verbose) VERBOSE=2 ; shift ;;
    --) shift ; break ;;
    *) echo "error 2 in getopt" >&2 ; exit 1 ;;
  esac
done

(

case "$ASPECT" in
  4:3) WIDE=0 ;;
  16:9) WIDE=1 ;;
  *) echo "aspect $ASPECT not recognized" >&2 ; exit 1 ;;
esac

case "$QUALITY" in
  5) WIDTH=32 ; VWIDTH=32 ;;
  6) WIDTH=64 ; VWIDTH=64 ;;
  7) WIDTH=128 ; VWIDTH=128 ;;
  8) WIDTH=256 ; VWIDTH=256 ;;
  9) WIDTH=512 ; VWIDTH=512 ;;
  10) WIDTH=1024 ; VWIDTH=960 ;;
  11) WIDTH=2048 ; VWIDTH=1920 ;;
  12) WIDTH=4096 ; VWIDTH=3840 ;;
  *) echo "quality $QUALITY not recognized" >&2 ; exit 1 ;;
esac

if [ $WIDE -eq 1 ]
then
  VHEIGHT=$((VWIDTH / 16 * 9))
else
  VHEIGHT=$((VWIDTH / 4 * 3))
fi

if [ $AUTO -eq 1 ]
then
  # FIXME
  TEMP="$("${E}/emndl_automu" "${AUTOBITS}" "${PERIOD}" "${REALCOORD}" "${IMAGCOORD}" 2>/dev/null | tee /dev/stderr | tail -n 1 | ( read b p r i s ; printf "%s\n" "${r} ${i} ${p}") )"
  REALCOORD="$(echo "${TEMP}" | sed 's|^\([^ ]*\) .*$|\1|')"
  IMAGCOORD="$(echo "${TEMP}" | sed 's|^[^ ]* \([^ ]*\) .*$|\1|')"
  PERIOD="$(echo "${TEMP}" | sed 's|^[^ ]* [^ ]* \([^ ]*\)$|\1|')"
  SIZE="$(m-size "${AUTOBITS}" "${REALCOORD}" "${IMAGCOORD}" "${PERIOD}" | cut -d\  -f 1)"
fi

if [ $VERBOSE -gt 0 ]
then
  cat << EOF
AUTO=$AUTO
AUTOBITS=$AUTOBITS
REAL=$REAL
REALCOORD=$REALCOORD
IMAG=$IMAG
IMAGCOORD=$IMAGCOORD
PER=$PER
PERIOD=$PERIOD
MAXITERS=$MAXITERS
SZ=$SZ
SIZE=$SIZE
QUALITY=$QUALITY
PNG=$PNG
JPEG=$JPEG
ASPECT=$ASPECT
SPEED=$SPEED
DVD=$DVD
MKV=$MKV
OGV=$OGV
VERBOSE=$VERBOSE

WIDE=$WIDE
WIDTH=$WIDTH
VWIDTH=$VWIDTH
VHEIGHT=$VHEIGHT
E=$E
OUT=$OUT
EOF
fi

mkdir "${OUT}"
cd "${OUT}"

if [ $VERBOSE -gt 0 ] ; then echo "calculating.." ; fi
"${E}/emndl_calculate" "out.emndl" "${WIDTH}" "${SIZE}" "${REALCOORD}" "${IMAGCOORD}" "${PERIOD}" "${MAXITERS}"

if [ $VERBOSE -gt 0 ] ; then echo "finalizing.." ; fi
"${E}/emndl_finalize" "out.emndl" "dwell-neq.f" "distance.f" "angle.f" "period.f" "newton.f"

if [ $VERBOSE -gt 0 ] ; then echo "equalizing.." ; fi
"${E}/emndl_equalize" "dwell-neq.f" "dwell.f"

if [ $VERBOSE -gt 0 ] ; then echo "computing depth.." ; fi
DEPTH="$(( $(stat -c '%s' "dwell.f") / (WIDTH * WIDTH * 4) ))"
if [ $VERBOSE -gt 0 ] ; then echo "DEPTH=${DEPTH}" ; fi

if [ $VERBOSE -gt 0 ] ; then echo "downscaling.." ; fi
for p in dwell distance angle period newton
do
  cat "${p}.f" "/dev/zero" | head -c "$(( WIDTH * WIDTH * (DEPTH + 1) * 4 ))" > "${p}-00.f"
done
for q in $(seq "0" "$((QUALITY - 1))")
do
  for p in dwell distance angle period newton
  do
    "${E}/emndl_downscale" "${p}" "$(( 1 << (QUALITY - q) ))" < "${p}-$(printf "%02d" "$((q))").f" > "${p}-$(printf "%02d" "$((q + 1))").f"
  done
done

if [ $VERBOSE -gt 0 ] ; then echo "colourizing.." ; fi
for q in $(seq 0 "${QUALITY}")
do
  f="$(printf "%02d" "${q}").f"
  r="${f%f}raw"
  "${E}/emndl_colourize" "dwell-${f}" "distance-${f}" "angle-${f}" "period-${f}" "newton-${f}" "${r}"
  w="$(( 1 << (QUALITY - q) ))"
  h="$(( w * (DEPTH + 1) ))"
  ( echo -e "P6\n${w} ${h} 255" && cat "${r}" ) > "emndl.${f%f}ppm"
done

if [ $VERBOSE -gt 0 ] ; then echo "making images.." ; fi
if [ $PNG  -eq 1 ] ; then convert "emndl.00.ppm" "out.png" ; fi
if [ $JPEG -eq 1 ] ; then convert "emndl.00.ppm" -thumbnail 64x "out.jpeg" ; fi

if [ $VERBOSE -gt 0 ] ; then echo "making videos.." ; fi

if [ $MKV -eq 1 ]
then
  (
    "${E}/emndl_unwarp" emndl "${VWIDTH}" "${VHEIGHT}" "${SPEED}" 2>audio.mkv.raw |
    "${E}/emndl_ppmtoy4m" "${VWIDTH}" "${VHEIGHT}" |
    ffmpeg -f yuv4mpegpipe -i - -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 video.mkv >/dev/null 2>&1
  ) &
fi

if [ $DVD -eq 1 ]
then
  ( "${E}/emndl_unwarp" emndl 1048 576 "${SPEED}" 2>audio.dvd.raw |
    "${E}/emndl_ppmtoy4m" 1048 576 |
    y4mscaler -v 0 -O preset=dvd_wide -O yscale=1/1 |
    mpeg2enc -v 0 -f 8 -q 3 -b 8000 -B 768 -D 9 -g 9 -G 15 -P -R 2 -o "video.m2v"
  ) &
fi

if [ $OGV -eq 1 ]
then
  ( "${E}/emndl_unwarp" emndl 512 288 "${SPEED}" 2>audio.ogv.raw |
    "${E}/emndl_ppmtoy4m" 512 288 |
    ffmpeg2theora -o video.ogv - >/dev/null 2>&1
  ) &
fi

wait

if [ $VERBOSE -gt 0 ] ; then echo "making soundtrack.." ; fi
if [ $((DVD + MKV + OGV)) -gt 0 ]
then
  audio_raw="$(ls -1 audio.???.raw | head -n 1)"
  ecasound -q -f:f32_le,2,48000,i -i:"${audio_raw}" -f:s16_le,2,48000,i -o:audio-raw.wav
  if [ "x$(pwd | tr -cd "[:space:]\\{}")" == "x" ]
  then
    pd -nrt -noaudio -noprefs -path . -r 48000 -batch -open "${E}/../share/emndl/emndl_loader.pd" -send "emndl audio-raw.wav $(pwd)/audio.wav"
  else
    echo "warning: dir \`$(pwd)' contains bad chars, aborting"
    exit
  fi
fi

if [ $DVD -eq 1 ]
then
  (
    twolame --quiet -b 224 audio.wav audio.mp2
    mplex -v 0 -f 8 -V -o out.mpeg video.m2v audio.mp2
  ) &
fi
if [ $MKV -eq 1 ]
then
  (
    flac --best --verify audio.wav -o audio.flac
    mkvmerge -o out.mkv video.mkv audio.flac
  ) &
fi
if [ $OGV -eq 1 ]
then
  (
    oggenc --quiet -b 192 -o audio.ogg audio.wav
    oggz-merge -o out.ogv video.ogv audio.ogg
  ) &
fi
wait

cd ..
TIME1="$(date --iso=s)"

if [ $VERBOSE -gt 0 ]
then
  echo "${TIME0}  begin"
  echo "${TIME1}  end"
fi

) 2>&1 | tee "${OUT}.log"

exit 0
