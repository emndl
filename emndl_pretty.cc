/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <qd/qd_real.h>
#include <qd/fpu.h>

int main(int argc, char **argv) {
  fpu_fix_start(0);
  qd_real q[2];
  std::cout << std::setprecision(64);
  while (2 == fread(q, sizeof(qd_real), 2, stdin)) {
    std::cout << q[0] << " " << q[1] << std::endl;
  }
  return 0;
}
