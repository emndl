{-
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module Main (main) where

import Control.Monad (guard, when)
import Data.Either (lefts)
import Data.List (minimumBy)
import Data.Maybe (listToMaybe, fromMaybe)
import Data.Ord (comparing)
import Data.Vec (NearZero(nearZero))
import System.Random (RandomGen, randomR, newStdGen) -- StdGen)
import System.Environment (getArgs)
import System.IO (hPutStrLn, stderr)
import Control.Parallel.Strategies (parMap, rseq)
import Data.Number.MPFR (toString, RoundMode(Near), set)
import Data.Number.MPFR.Instances.Near()

import Number (I, R, C)
import Complex (Complex((:+)), magnitude2)
import MuAtom (refineNucleus)
import GridScan (gridEdge, gridShow, gridConverge, gridStep, gridScan, gridSpace)

default (Int)

straddlesOrigin :: [C] -> Bool
straddlesOrigin ps = odd . length . filter id . zipWith positiveReal ps $ (drop 1 ps ++ take 1 ps)

positiveReal :: C -> C -> Bool
positiveReal (u:+v) (x:+y)
  | v < 0 && y < 0 = False
  | v > 0 && y > 0 = False
  | (u * (y - v) - v * (x - u)) * (y - v) > 0 = True
  | otherwise = False

maxPeriod :: I -> I
maxPeriod p = 12 * p

locateNucleus :: I -> R -> C -> Maybe I
locateNucleus p r c =
  let cs = [ c + (r:+r), c + (r:+(-r)), c + ((-r):+(-r)), c + ((-r):+r) ]
      zs = iterate (zipWith (\cc z -> z * z + cc) cs) [0,0,0,0]
  in  fmap fst . listToMaybe . dropWhile (not . straddlesOrigin . snd) . zip [0 .. maxPeriod p] $ zs

offsetFromOrigin :: [C] -> R
offsetFromOrigin = magnitude2 . sum

rescan :: I -> R -> C -> Maybe (R, C)
rescan p r0 c0
  = case filter (straddlesOrigin . snd)
  . parMap rseq (fmap (parMap rseq (\c -> iterate (\z->z * z + c) 0 !! p)))
  . fmap (\(r, c) -> ((r, c), [c + (r:+r), c + ((-r):+r), c + ((-r):+(-r)), c + (r:+(-r))]))
  $( [ (r', c0 + d) | i <- [-1,1], j <- [-1,1 ], let d = ((r'*i) :+ (r'*j)) ]
  ++ [ (r', c0 + d) | i <- [  0 ], j <- [  0 ], let d = ((r'*i) :+ (r'*j)) ]) of
    [] -> Nothing
    xs -> Just . fst . minimumBy (comparing $ offsetFromOrigin . snd) $ xs
  where r' = r0 / 2

shuffle :: RandomGen g => [a] -> g -> ([a], g)
shuffle [] g = ([], g)
shuffle xs g =  let (i, g') = randomR (0, length xs - 1) g
                    (h, x:t) = splitAt i xs
                    (xs', g'') = shuffle (h ++ t) g'
                in  (x : xs', g'')

autotunes :: RandomGen g => ((C, R, I), g) -> [Either (C, R, I) String]
autotunes x@((c0, r0, p0), _) = Left (c0, r0, p0) :
  case autotune1 x of
    (Nothing,  _) -> []
    (Just str, m) -> Right str : case m of
      Nothing -> []
      Just y -> autotunes y

autotune1 :: RandomGen g => ((C, R, I), g) -> (Maybe String, Maybe ((C, R, I), g))
autotune1 (x@(c0, r0, _), g0)
  | nearZero r0 = (Nothing, Nothing)
  | otherwise =
      let grid = gridConverge . iterate gridStep . gridScan c0 $ r0
          edge = gridEdge grid
          str = gridShow grid
          (es, g1) = shuffle edge g0
      in  (Just str, autotune' es (x, g1))

autotune' :: RandomGen g => [C] -> ((C, R, I), g) -> Maybe ((C, R, I), g)
autotune' [] _ = Nothing
autotune' (c1:es) x@((_, r0, p0), g0) =
  fromMaybe (autotune' es x) $ do
    let r1 = gridSpace r0
    p1 <- locateNucleus p0 r1 c1
    guard $ p1 > 2 && (p0 == 1 || p1 `mod` p0 /= 0)
    (_, c1') <- (!! 64) . iterate (uncurry (rescan p1) =<<) $ Just (r1, c1)
    let (re, im, r2) = refineNucleus (fromIntegral p1) c1'
        c2 = re :+ im
    return $ Just ((c2, r2, p1), g0)

main :: IO ()
main = do
  args <- getArgs
  let bits = case args of
        [b] -> read b
        _ -> 128 :: Int
      dec = ceiling . ((logBase 10 2 :: Double) *) . fromIntegral $ bits
      r = set Near (fromIntegral bits)
--      g = read "123456789 987654321" :: StdGen
  g <- newStdGen
  let ats = autotunes ((r 0 :+ r 0, r 1, 1), g)
      (fre :+ fim, fzr, _) = last (lefts ats)
      pretty (Left (re:+im,zr,p)) = ["P " ++ show p, "R " ++ toString dec re, "I " ++ toString dec im, "@ " ++ toString dec zr]
      pretty (Right s) = [s]
  when verbose $ (mapM_ (hPutStrLn stderr) . concatMap pretty . init) ats
  putStrLn . unwords $ [ toString dec fre, toString dec fim, show (ceiling (0.5 + logBase 2 (256 / fzr) / 8) :: Int) ]

verbose :: Bool
verbose = True
