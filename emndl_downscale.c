/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011,2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if (argc != 3) exit(1);
  int m = -1;
  if (strcmp("dwell",    argv[1]) == 0) m = 0; else
  if (strcmp("distance", argv[1]) == 0) m = 1; else
  if (strcmp("angle",    argv[1]) == 0) m = 0; else
  if (strcmp("period",   argv[1]) == 0) m = 0; else
  if (strcmp("newton",   argv[1]) == 0) m = 0;
  if (m == -1) exit(1);
  int iwidth = atoi(argv[2]);
  int owidth = iwidth >> 1;
  float *iline[2];
  float *oline;
  int ibytes = iwidth * sizeof(float);
  int obytes = owidth * sizeof(float);
  iline[0] = (float *) malloc(ibytes);
  iline[1] = (float *) malloc(ibytes);
  oline    = (float *) malloc(obytes);   
  while ((1 == fread(iline[0], ibytes, 1, stdin)) &&
         (1 == fread(iline[1], ibytes, 1, stdin))) {
    for (int ox = 0; ox < owidth; ++ox) {
      int i = ox << 1;
      int j = i + 1;
      int k = 0;
      float o;
      if (m == 0) {
        o = 0.0;
        if (iline[0][i] > 0) { k += 1; o += iline[0][i]; }
        if (iline[0][j] > 0) { k += 1; o += iline[0][j]; }
        if (iline[1][i] > 0) { k += 1; o += iline[1][i]; }
        if (iline[1][j] > 0) { k += 1; o += iline[1][j]; }
        if (k > 0) {
          o /= k;
        }
      } else {
        o = 1.0;
        if (iline[0][i] > 0) { k += 1; o *= iline[0][i]; }
        if (iline[0][j] > 0) { k += 1; o *= iline[0][j]; }
        if (iline[1][i] > 0) { k += 1; o *= iline[1][i]; }
        if (iline[1][j] > 0) { k += 1; o *= iline[1][j]; }
        if (k > 1) {
          o = 0.5 * powf(o, 1.0 / k);
        } else {
          o = 0.0;
        }
      }
      oline[ox] = o;
    }
    if (fwrite(oline, obytes, 1, stdout) != 1) exit(1);
  }
  free(iline[0]);
  free(iline[1]);
  free(oline);
  return 0;
}
