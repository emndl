{-
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module Number (I, N, Q, R, C) where

import Data.Vec (NearZero(..))
import Data.Number.MPFR (MPFR, getPrec, int2w, RoundMode(Up))
import Data.Number.MPFR.Instances.Near ()
{-
import System.Random (Random(..))
import Data.Ratio ((%))
-}

import Complex (Complex())

type I = Int
type N = Integer
type Q = Rational
type R = MPFR
type C = Complex R

instance NearZero MPFR where
  nearZero x = let p = getPrec x in not (abs x > int2w Up p 1 (4 - fromIntegral p))

{-
instance Random MPFR where
  random g0 = let k = 2 ^ (300 :: Int) :: Integer
                  (m, g1) = randomR (0, k) g0
              in  (fromRational (m % k), g1)
  randomR (lo, hi) g0 = let (r, g1) = random g0
                        in  (lo + (hi - lo) * r, g1)
-}
