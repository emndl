{-
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011,2018  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module Complex where

-- this is ugly: can't use Data.Complex because no RealFloat for MPFR
data Complex c = !c :+ !c deriving (Read, Show, Eq)

-- complex number arithmetic, with extra strictness and cost-centres
instance Num c => Num (Complex c) where
  (a :+ b) + (c :+ d) = (a + c) :+ (b + d)
  (a :+ b) - (c :+ d) = (a - c) :+ (b - d)
  (a :+ b) * (c :+ d) = (a * c - b * d) :+ (a * d + b * c)
  negate (a :+ b) = negate a :+ negate b
  abs x = error $ "Complex.abs"
  signum x = error $ "Complex.signum"
  fromInteger x = fromInteger x :+ 0

instance (Fractional a) => Fractional (Complex a)  where
  (a:+b) / (c:+d) = ((a*c + b*d) / m) :+ ((b*c - a*d) / m) where m = c*c + d*d
  fromRational a = fromRational a :+ 0
    
magnitude :: (Floating c) => Complex c -> c
magnitude = sqrt . magnitude2

magnitude2 :: (Num c) => Complex c -> c
magnitude2 (re:+im) = re * re + im * im

cis :: Floating c => c -> Complex c
cis a = cos a :+ sin a

mkPolar :: Floating c => c -> c -> Complex c
mkPolar r a = (r * cos a) :+ (r * sin a)

phase :: (Floating c, Ord c) => Complex c -> c
phase (re:+im) = atan2' im re

atan2' :: (Floating c, Ord c) => c -> c -> c
atan2' y x
  | x > 0            =  atan (y / x)
  | x == 0 && y > 0  =  pi/2
  | x <  0 && y > 0  =  pi + atan (y / x)
  | x <= 0 && y < 0  = -atan2' (-y) x
  | y == 0 && x < 0  =  pi
  | x == 0 && y == 0 =  y
  | otherwise = error "Complex.atan2'"

normalize :: Floating c => Complex c -> Complex c
normalize z@(re:+im) = let m = magnitude z in (re / m) :+ (im / m)
