/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <qd/qd_real.h>

#define likely(x)   __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

qd_real exterior_distance(const qd_real &cx, const qd_real &cy, int *n, int count, const qd_real &r2) {
  qd_real px = 0;
  qd_real py = 0;
  qd_real dx = 1;
  qd_real dy = 0;
  int escape = 0;
  while (likely(count--)) {
    qd_real px2 = px * px;
    qd_real py2 = py * py;
    qd_real d2 = px2 + py2;
    if (d2 > r2) { escape = 1; break; }
    qd_real pxy = px * py;
    qd_real px1 = px2 - py2 + cx;
    qd_real py1 = 2 * pxy + cy;
    qd_real dx1 = 2 * (px * dx - py * dy) + 1;
    qd_real dy1 = 2 * (dx * py + dy * px);
    px = px1; py = py1; dx = dx1; dy = dy1;
    ++(*n);
  }
  if (escape) {
    qd_real p = sqrt(px * px + py * py);
    qd_real d = sqrt(dx * dx + dy * dy);
    return 2 * p * log(p) / d;
  } else {
    return -1;
  }
}

void random_interesting(qd_real &re, qd_real &im, qd_real &d) {
  int interesting = 0;
  qd_real x;
  qd_real y;
  while (likely(! interesting)) {
    x = re + d * 2.0 * (qdrand() - 0.5);
    y = im + d * 2.0 * (qdrand() - 0.5);
    int n = 0;
    qd_real e = exterior_distance(x, y, &n, 1024, 65536);
    if (0 < e && e < d) {
      re = x;
      im = y;
      d = e;
      std::cerr << std::setprecision(64) << re << " " << im << " " << std::setprecision(8) << d << "\r";
    }
    interesting = 0 < e && e < pow(qd_real(2.0), int(-180));
  }
}

int main(int argc, char **argv) {
  fpu_fix_start(0);
  srand(time(0));
  qd_real re(0.0);
  qd_real im(0.0);
  qd_real d(2.0);
  random_interesting(re, im, d);
  std::cerr << std::endl;
  fwrite(&re, sizeof(qd_real), 1, stdout);
  fwrite(&im, sizeof(qd_real), 1, stdout);
  return 0;
}
