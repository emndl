{-
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module GridScan (gridEdge, gridShow, gridConverge, gridStep, gridScan, gridSpace) where

import Control.Parallel.Strategies (parMap, rseq)
import Data.Array.IArray (Array, listArray, IArray(bounds), inRange, assocs, elems, indices, (!))
import Data.Maybe (catMaybes, isJust, isNothing)
import Data.Number.MPFR (set, Precision, RoundMode(Near), sqr, getPrec, mulw)

import Number (I, R, C)
import Complex (Complex ((:+)), magnitude2)

data GridScan = GridScan I (Array (I,I) (Maybe C)) (I -> I -> C)

gridSize :: I
gridSize = 64

gridRadius :: R
gridRadius = 2

gridOffset :: I
gridOffset = gridSize `div` 2

gridScan :: C -> R -> GridScan
gridScan c0 r0 = GridScan 0 (listArray ((0,0),(gridSize - 1, gridSize - 1)) (replicate (gridSize * gridSize) (Just z))) f
  where
    s = gridSpace r0
    p = ceiling $ 5 - logBase 2 s
    z = set Near p 0 :+ set Near p 0
    f i j =
      let x = s * (fromIntegral (i - gridOffset))
          y = s * (fromIntegral (j - gridOffset))
          re :+ im = c0 + (x :+ y)
      in  set Near p re :+ set Near p im

gridSpace :: R -> R
gridSpace r0 = r0 * gridRadius / fromIntegral gridOffset

gridStep :: GridScan -> GridScan
gridStep (GridScan n pixels coords) =
  let m = (5 * n `div` 4) `max` 1000
      d = m - n
      g _ _ o@Nothing = o
      g k0 c@(r:+_) (Just z0) = go k0 z0
        where
          p = getPrec r
          go 0 z = z `seq` Just z
          go k z = z `seq` let z' = sqr' p z + c in if magnitude2 z' > 4 then Nothing else go (k - 1) z'
      ps = parMap rseq (\((i,j),e) -> g d (coords i j) e) . assocs $ pixels
      pixels' = listArray (bounds pixels) ps
  in  GridScan m pixels' coords

sqr' :: Precision -> C -> C
sqr' p (r:+i) =
  let r2 = sqr Near p r
      i2 = sqr Near p i
      ri = mulw Near p (r * i) 2
  in  (r2 - i2) :+ ri

gridConverge :: [GridScan] -> GridScan
gridConverge (GridScan _ p _ : gs@(g@(GridScan _ q _) : _))
  | fp == m = gridConverge gs
  | fp == f q = g
  | otherwise = gridConverge gs
  where
    fp = f p
    f = length . catMaybes . elems
    m = gridSize * gridSize
gridConverge _ = error "GridScan.gridConverge: list too short"

gridEdge :: GridScan -> [C]
gridEdge (GridScan _ pixels coords) =
  let b = bounds pixels
      edge (i, j) = isNothing (pixels ! (i, j)) && any isJust [pixels ! (i',j') | i' <- [i-2 .. i+2], j' <- [j-2 .. j+2], inRange b (i',j')]
  in  map (uncurry coords) . filter edge . indices $ pixels

gridShow :: GridScan -> String
gridShow (GridScan _ a _) = unlines [ [ block (isJust (a ! (i,j))) (isJust (a ! (i, j - 1))) | i <- [i0 .. i1] ] | j <- [j1, j1 - 2 .. j0] ]
  where
    ((i0,j0),(i1,j1)) = bounds a
    block False False = ' '
    block True False = '\x2580'
    block False True = '\x2584'
    block True True = '\x2588'
