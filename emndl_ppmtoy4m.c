/*
emndl_ppmtoy4m -- faster PPM to YUV4MPEG2 conversion
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Portions of this code are derived from mjpegtools:
"mjpeg/mjpeg_play/lavtools/colorspace.c"

colorspace.c: Routines to perform colorspace conversions.
Copyright (C) 2001 Matthew J. Marjanovic <maddog@mir.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FP_BITS 18

static inline int myround(double n) {
  if (n >= 0) return (int)(n + 0.5);
  else        return (int)(n - 0.5);
}

#define Y 0
#define U 1
#define V 2
#define R 0
#define G 1
#define B 2

int main(int argc, char **argv) {
  if (argc != 3) { return 1; }
  char *widths = argv[1];
  char *heights = argv[2];
  char ppmtemplate[1024];
  snprintf(ppmtemplate, 1000, "P6\n%s %s 255\n", widths, heights);
  int width;
  int height;
  if (sscanf(ppmtemplate, "P6\n%d %d 255\n", &width, &height) != 2) { fprintf(stderr, "ppm template parse\n"); return 1; }
  int n = width * height;
  unsigned char *rgbs = malloc(n * 3);
  unsigned char *ys = malloc(n);
  unsigned char *us = malloc(n);
  unsigned char *vs = malloc(n);
  char y4m[1024];
  snprintf(y4m, 1000, "YUV4MPEG2 W%d H%d F60:1 Ip A1:1 C444\n", width, height);
  if (fwrite(y4m, strlen(y4m), 1, stdout) != 1) { fprintf(stderr, "y4m header write\n"); return 1; }
  int32_t cc[3][3][256];
  for (int i = 0; i < 256; ++i) {
    cc[Y][R][i] = myround(0.299 * (double)i * 219.0 / 255.0 * (double)(1<<FP_BITS));
    cc[Y][G][i] = myround(0.587 * (double)i * 219.0 / 255.0 * (double)(1<<FP_BITS));
    cc[Y][B][i] = myround((0.114 * (double)i * 219.0 / 255.0 * (double)(1<<FP_BITS)) + (double)(1<<(FP_BITS-1)) + (16.0 * (double)(1<<FP_BITS)));
    cc[U][R][i] = myround(-0.168736 * (double)i * 224.0 / 255.0 * (double)(1<<FP_BITS));
    cc[U][G][i] = myround(-0.331264 * (double)i * 224.0 / 255.0 * (double)(1<<FP_BITS));
    cc[U][B][i] = myround((0.500 * (double)i * 224.0 / 255.0 * (double)(1<<FP_BITS)) + (double)(1<<(FP_BITS-1)) + (128.0 * (double)(1<<FP_BITS)));
    cc[V][R][i] = myround(0.500 * (double)i * 224.0 / 255.0 * (double)(1<<FP_BITS));
    cc[V][G][i] = myround(-0.418688 * (double)i * 224.0 / 255.0 * (double)(1<<FP_BITS));
    cc[V][B][i] = myround((-0.081312 * (double)i * 224.0 / 255.0 * (double)(1<<FP_BITS)) + (double)(1<<(FP_BITS-1)) + (128.0 * (double)(1<<FP_BITS)));
  }
  int ppml = strlen(ppmtemplate);
  char ppm[1024];
  while (fread(ppm, ppml, 1, stdin) == 1) {
    ppm[ppml] = '\0';
    if (strncmp(ppmtemplate, ppm, ppml)) { fprintf(stderr, "ppm template mismatch\n%s%s", ppmtemplate, ppm); return 1; }
    if (fread(rgbs, n * 3, 1, stdin) != 1) { fprintf(stderr, "ppm read mismatch\n"); return 1; }
    unsigned char *rgb = rgbs;
    unsigned char *y = ys;
    unsigned char *u = us;
    unsigned char *v = vs;
    for (int i = 0; i < n; ++i) {
      int r = *rgb++;
      int g = *rgb++;
      int b = *rgb++;
      *y++ = (cc[Y][R][r] + cc[Y][G][g] + cc[Y][B][b]) >> FP_BITS;
      *u++ = (cc[U][R][r] + cc[U][G][g] + cc[U][B][b]) >> FP_BITS;
      *v++ = (cc[V][R][r] + cc[V][G][g] + cc[V][B][b]) >> FP_BITS;
    }
    if (fwrite("FRAME\n", 6, 1, stdout) != 1) { fprintf(stderr, "y4m frame write\n"); return 1; }
    if (fwrite(ys, n, 1, stdout) != 1) { fprintf(stderr, "y4m y write\n"); return 1; }
    if (fwrite(us, n, 1, stdout) != 1) { fprintf(stderr, "y4m u write\n"); return 1; }
    if (fwrite(vs, n, 1, stdout) != 1) { fprintf(stderr, "y4m v write\n"); return 1; }
  }
  return 0;
}
