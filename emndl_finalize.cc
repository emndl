/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011,2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv) {
  if (argc != 7) exit(1);
  FILE *fdi = fopen(argv[1], "rb");
  FILE *fddwell = fopen(argv[2], "wb");
  FILE *fddistance = fopen(argv[3], "wb");
  FILE *fdangle = fopen(argv[4], "wb");
  FILE *fdperiod = fopen(argv[5], "wb");
  FILE *fdnewton = fopen(argv[6], "wb");
  float dda[5];
  while (1 == fread(dda, 5 * sizeof(float), 1, fdi)) {
    if (1 != fwrite(&dda[0], sizeof(float), 1, fddwell)) exit(1);
    if (1 != fwrite(&dda[1], sizeof(float), 1, fddistance)) exit(1);
    if (1 != fwrite(&dda[2], sizeof(float), 1, fdangle)) exit(1);
    if (1 != fwrite(&dda[3], sizeof(float), 1, fdperiod)) exit(1);
    if (1 != fwrite(&dda[4], sizeof(float), 1, fdnewton)) exit(1);
  }
  fclose(fdi);
  fclose(fddwell);
  fclose(fddistance);
  fclose(fdangle);
  fclose(fdperiod);
  fclose(fdnewton);
  return 0;
}
