/*
    emndl -- exponentially transformed Mandelbrot Set renderer
    Copyright (C) 2011,2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <math.h>

float lin_interp(float x, float domain_low, float domain_hi, float range_low, float range_hi) {
  if ((x >= domain_low) && (x <= domain_hi)) {
    x = (x - domain_low) / (domain_hi - domain_low);
    x = range_low + x * (range_hi - range_low);
  }
  return x;
}

float pvp_adjust_3(float x) {
  // red
  x = lin_interp(x, 0.00, 0.125, -0.050, 0.090);
  // orange
  x = lin_interp(x, 0.125, 0.25,  0.090, 0.167);
  // yellow
  x = lin_interp(x, 0.25, 0.375,  0.167, 0.253);
  // chartreuse
  x = lin_interp(x, 0.375, 0.50,  0.253, 0.383);
  // green
  x = lin_interp(x, 0.50, 0.625,  0.383, 0.500);
  // teal
  x = lin_interp(x, 0.625, 0.75,  0.500, 0.667);
  // blue
  x = lin_interp(x, 0.75, 0.875,  0.667, 0.800);
  // purple
  x = lin_interp(x, 0.875, 1.00,  0.800, 0.950);
  return(x);
}

void hsv2rgb(float h, float s, float v, float *rp, float *gp, float *bp)
{
  float i, f, p, q, t, r, g, b;
  int ii;
  if (s == 0.0) {
    // Ignore hue
    r = v;
    g = v;
    b = v;
  } else {
    /* Apply physiological mapping: red, yellow, green and blue should
       be equidistant. */
    h = pvp_adjust_3(h);
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h); ii = (int) i;
    f = h - i;
    p = v*(1.0 - s);  // The "low-flat" curve
    q = v*(1.0 - (s*f));  // The "falling" curve
    t = v*(1.0 - (s*(1.0 - f)));  // The "rising" curve
    switch(ii) {
      case 0:  // red point
        r = v; g = t; b = p; // RED max, GRN rising, BLU min
        break;
      case 1:  // yellow point
        r = q; g = v; b = p; // RED falling, GRN max, BLU min
        break;
      case 2:  // green point
        r = p; g = v; b = t; // RED min, GRN max, BLU rising
        break;
      case 3:  // cyan point
        r = p; g = q; b = v; // RED min, GRN falling, BLU max
        break;
      case 4:  // blue point
        r = t; g = p; b = v; // RED rising, GRN min, BLU max
        break;
      case 5:  // magenta point
      default:
        r = v; g = p; b = q; // RED max, GRN min, BLU falling
        break;
    }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

int main(int argc, char **argv) {

  if (argc != 7) exit(1);

  FILE *fddwell    = fopen(argv[1], "rb");
  FILE *fddistance = fopen(argv[2], "rb");
  FILE *fdangle    = fopen(argv[3], "rb");
  FILE *fdperiod   = fopen(argv[4], "rb");
  FILE *fdnewton   = fopen(argv[5], "rb");
  struct stat s;
  if (stat(argv[1], &s)) exit(1);
  int n = s.st_size / sizeof(float);

  FILE *fdo = fopen(argv[6], "wb");

  for (int i = 0; i < n; ++i) {
    float dwell, distance, angle, period, newton;
    if (1 != fread(&dwell,    sizeof(float), 1, fddwell   )) exit(1);
    if (1 != fread(&distance, sizeof(float), 1, fddistance)) exit(1);
    if (1 != fread(&angle,    sizeof(float), 1, fdangle   )) exit(1);
    if (1 != fread(&period,   sizeof(float), 1, fdperiod  )) exit(1);
    if (1 != fread(&newton,   sizeof(float), 1, fdnewton  )) exit(1);
    float h = dwell;
    float s = 0.5;
    float v = 0.25 + log2f(distance) / 4;
    v = fminf(fmaxf(v, 0), 1);
    if (h <= 0) {
      s = 0;
      v = 1;
    }
    s = 0;
    float r, g, b;
    hsv2rgb(h, s, v, &r, &g, &b);
    unsigned char rgb[3];
    rgb[0] = fminf(fmaxf(255 * r, 0), 255);
    rgb[1] = fminf(fmaxf(255 * g, 0), 255);
    rgb[2] = fminf(fmaxf(255 * b, 0), 255);
    if (1 != fwrite(rgb, 3, 1, fdo)) exit(1);
  }

  fclose(fdo);
  fclose(fddwell);
  fclose(fddistance);
  fclose(fdangle);
  fclose(fdperiod);
  fclose(fdnewton);

  return 0;
}
